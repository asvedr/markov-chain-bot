use std::env;

use rust_stemmers::{Algorithm, Stemmer};

fn main() {
    let stemmer = Stemmer::create(Algorithm::Russian);
    let mut args = env::args().collect::<Vec<_>>();
    args.remove(0);
    for word in args {
        let stem = stemmer.stem(&word);
        println!("{:?} => {:?}", word, stem);
    }
}

