use mddd::macros::IConfig;

#[derive(IConfig)]
pub struct Config {
    #[default(100)]
    pub tag_repo_bulk_size: usize,
    #[default(100)]
    pub word_repo_bulk_size: usize,
    #[default(1000)]
    pub text_compiler_bulk_size: usize,
}
