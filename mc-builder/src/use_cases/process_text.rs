use std::fmt;
use std::fs;
use std::io;
use std::io::{Error, Read};
use std::ops::Range;

use easy_sqlite::errors::DbError;
use mc_base::entities::common::Lang;
use mc_base::entities::constants::SENTENCE_TERMINATORS;
use mc_base::entities::meta::Ranges;
use mc_base::entities::tag::Tag;
use mc_base::entities::tokens::Token;
use mc_base::proto::db::ISettingsRepo;
use mddd::macros::IUseCaseRequest;
use mddd::traits::IUseCase;

use crate::proto::prepare::{ITagger, ITokenSeparator};
use crate::proto::process::{ITokenProcessor, IWordProcessor, TokenProcessorRequest};
use crate::use_cases::common::RangeParser;

pub struct ProcessTextUC {
    pub separator_factory: &'static dyn Fn(Lang) -> &'static dyn ITokenSeparator,
    pub tagger_factory: &'static dyn Fn(Lang) -> &'static dyn ITagger,
    pub processor_factory: &'static dyn Fn(&str) -> Box<dyn ITokenProcessor>,
    pub word_processor_factory: &'static dyn Fn(&str) -> Box<dyn IWordProcessor>,
    pub settings_repo_factory: &'static dyn Fn(&str) -> Box<dyn ISettingsRepo>,
}

#[derive(IUseCaseRequest)]
pub struct Request {
    #[short("-d")]
    db_path: String,
    #[short("-t")]
    text_path: String,
    #[short("-l")]
    #[default(Lang::Ru)]
    lang: Lang,
    #[short("-T")]
    #[multi]
    force_tags: Vec<Tag>,
    #[long("--only_from_begin")]
    #[default(1..4)]
    #[parser(RangeParser)]
    only_from_begin: Range<usize>,
    #[long("--with_tags")]
    #[default(0..0)]
    #[parser(RangeParser)]
    with_tags: Range<usize>,
    #[long("--without_tags")]
    #[default(3..4)]
    #[parser(RangeParser)]
    without_tags: Range<usize>,
}

pub struct UseCaseError {
    msg: String,
}

impl ProcessTextUC {
    fn parse_tokens(
        &self,
        path: String,
        separator: &dyn ITokenSeparator,
    ) -> Result<Vec<Token>, UseCaseError> {
        let mut buf = String::new();
        fs::File::open(path)?.read_to_string(&mut buf)?;
        println!("text is read");
        let result = separator.separate(&buf);
        println!("tokenized");
        Ok(result)
    }

    fn warn_about_bad_sequence(&self, tokens: &[Token], index_of_end: usize) {
        let limit = 10;
        let seq = if index_of_end <= limit {
            &tokens[..index_of_end]
        } else {
            &tokens[index_of_end - limit..index_of_end]
        };
        let msg = seq
            .iter()
            .map(|tkn| format!("{}", tkn))
            .collect::<Vec<_>>()
            .join(" ");
        eprintln!("warning, possible broken seq: {}", msg)
    }

    fn warn_about_bad_sequences(&self, tokens: &[Token]) {
        for i in 0..tokens.len() - 1 {
            match (&tokens[i], &tokens[i + 1]) {
                (Token::Word(_), Token::EndOfSentence) => {
                    self.warn_about_bad_sequence(tokens, i + 1)
                }
                (Token::Punctuation(sym), Token::EndOfSentence)
                    if !SENTENCE_TERMINATORS.contains(*sym) =>
                {
                    self.warn_about_bad_sequence(tokens, i + 1)
                }
                _ => (),
            }
        }
    }
}

impl IUseCase for ProcessTextUC {
    type Request = Request;
    type Error = UseCaseError;

    fn short_description() -> String {
        "process text".to_string()
    }

    fn description(&self) -> String {
        concat!(
            "process text and save to db\n",
            "allowed syntax for range:\n",
            "  a .. b - from [a to b)\n",
            "  a ..= b - from [a to b]\n",
            "  a - from [a to a]\n",
            "  off - from [0 to 0]\n",
            "  no - from [0 to 0]\n",
        )
        .to_string()
    }

    fn execute(&mut self, request: Request) -> Result<(), Self::Error> {
        let separator: &dyn ITokenSeparator = (self.separator_factory)(request.lang);
        let processor: Box<dyn ITokenProcessor> = (self.processor_factory)(&request.db_path);
        let tagger: &dyn ITagger = (self.tagger_factory)(request.lang);
        let settings_repo = (self.settings_repo_factory)(&request.db_path);
        settings_repo.set_lang(request.lang)?;
        let word_processor: Box<dyn IWordProcessor> =
            (self.word_processor_factory)(&request.db_path);
        let tokens = self.parse_tokens(request.text_path, separator)?;
        self.warn_about_bad_sequences(&tokens);
        word_processor.update_words(&tokens)?;
        println!("words updated");
        println!("tagging (force: {:?})", request.force_tags);
        let tagged = tagger.reveal_tags(tokens, request.force_tags);
        println!("tagged");
        processor.process_tokens(TokenProcessorRequest {
            tokens: &tagged,
            only_from_begin: request.only_from_begin,
            with_tags: request.with_tags.clone(),
            without_tags: request.without_tags.clone(),
        })?;
        println!("data saved");
        settings_repo.set_ranges(Ranges {
            tagged: request.with_tags,
            untagged: request.without_tags,
        })?;
        println!("completed");
        Ok(())
    }
}

impl fmt::Debug for UseCaseError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.msg)
    }
}

impl From<DbError> for UseCaseError {
    fn from(value: DbError) -> Self {
        Self {
            msg: format!("{:?}", value),
        }
    }
}

impl From<io::Error> for UseCaseError {
    fn from(value: Error) -> Self {
        Self {
            msg: format!("{:?}", value),
        }
    }
}
