use std::ops::Range;
use std::str::FromStr;

use mddd::traits::IParser;

pub struct RangeParser;

fn parse_usize(src: &str) -> Result<usize, String> {
    usize::from_str(src).map_err(|_| format!("invalid number: {:?}", src))
}

impl IParser<Range<usize>> for RangeParser {
    fn parse(src: &str) -> Result<Range<usize>, String> {
        if src == "no" || src == "off" {
            return Ok(0..0);
        }
        if let Ok(val) = usize::from_str(src) {
            return Ok(val..val + 1);
        }
        let split = src.split("..=").collect::<Vec<_>>();
        if split.len() == 2 {
            let begin = parse_usize(split[0])?;
            let end = parse_usize(split[1])? + 1;
            return Ok(begin..end);
        }
        let split = src.split("..").collect::<Vec<_>>();
        if split.len() != 2 {
            return Err("range must contain '..' or '..='".to_string());
        }
        let begin = parse_usize(split[0])?;
        let end = parse_usize(split[1])?;
        Ok(begin..end)
    }
}
