use std::fmt;
use std::ops::Range;

use easy_sqlite::errors::DbError;
use mc_base::entities::common::Lang;
use mc_base::entities::errors::{GeneratorError, TextCompilerError};
use mc_base::entities::tag::Tag;
use mc_base::proto::common::IStemmer;
use mc_base::proto::db::ISettingsRepo;
use mc_base::proto::gen::{GenSeqRequest, IGenerator, ITextCompiler};
use mddd::macros::IUseCaseRequest;
use mddd::traits::IUseCase;

use crate::use_cases::common::RangeParser;

pub struct GenerateUC {
    pub generator_factory: &'static dyn Fn(&str) -> Box<dyn IGenerator>,
    pub compiler_factory: &'static dyn Fn(&str) -> Box<dyn ITextCompiler>,
    pub settings_factory: &'static dyn Fn(&str) -> Box<dyn ISettingsRepo>,
    pub stemmer_factory: &'static dyn Fn(Lang) -> Box<dyn IStemmer>,
}

#[derive(IUseCaseRequest)]
pub struct Request {
    #[short("-d")]
    db_path: String,
    #[short("-T")]
    #[multi]
    tags: Vec<Tag>,
    #[long("--seq-window-size")]
    #[default(3)]
    seq_window_size: usize,
    #[long("--tagged-range")]
    #[default(0..4)]
    #[parser(RangeParser)]
    tagged_range: Range<usize>,
    #[long("--try-end-after-len")]
    try_end_after_len: Option<usize>,
    #[long("--use-prob")]
    #[description("use probability in random choice")]
    #[default(true)]
    use_prob: bool,
}

pub struct Error(String);

fn stem_tag(stemmer: &dyn IStemmer, tag: Tag) -> Tag {
    if tag.key.is_empty() {
        return tag;
    }
    Tag {
        tag_type: tag.tag_type,
        key: stemmer.stem(&tag.key).to_string(),
    }
}

impl IUseCase for GenerateUC {
    type Request = Request;
    type Error = Error;

    fn short_description() -> String {
        "gen text".to_string()
    }

    fn execute(&mut self, request: Request) -> Result<(), Error> {
        let gen: Box<dyn IGenerator> = (self.generator_factory)(&request.db_path);
        let settings = (self.settings_factory)(&request.db_path);
        let stemmer = (self.stemmer_factory)(settings.get_lang()?);
        let tags = request
            .tags
            .into_iter()
            .map(|t| stem_tag(&*stemmer, t))
            .collect::<Vec<_>>();
        let tokens = gen.gen_seq(
            GenSeqRequest::new()
                .set_tags(&tags)
                .set_seq_window_size(request.seq_window_size)
                .set_use_tag_if(&|seq| request.tagged_range.contains(&seq.len()))
                .set_use_prob_in_random_if(&|_| request.use_prob)
                .set_try_end_after_len(request.try_end_after_len),
        )?;
        let compiler: Box<dyn ITextCompiler> = (self.compiler_factory)(&request.db_path);
        let text = compiler.compile(tokens)?;
        println!("{}", text);
        Ok(())
    }
}

impl From<GeneratorError> for Error {
    fn from(value: GeneratorError) -> Self {
        Self(format!("generator: {:?}", value))
    }
}

impl From<TextCompilerError> for Error {
    fn from(value: TextCompilerError) -> Self {
        Self(format!("text compiler: {:?}", value))
    }
}

impl From<DbError> for Error {
    fn from(value: DbError) -> Self {
        Self(format!("db: {:?}", value))
    }
}

impl fmt::Debug for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}
