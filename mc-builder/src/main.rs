mod app;
mod entities;
mod impls;
mod proto;
mod use_cases;

fn main() {
    use crate::app::entrypoints::*;
    use mddd::std::{LeafRunner, NodeRunner};

    NodeRunner::new("chain builder")
        .add("proc", LeafRunner::new(process_text))
        .add("gen", LeafRunner::new(generate))
        .run()
}
