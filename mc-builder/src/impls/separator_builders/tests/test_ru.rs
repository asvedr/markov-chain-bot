use mc_base::entities::tokens::Token;

use crate::impls::separator_builders::ru;
use crate::proto::prepare::ITokenSeparator;

fn separator() -> Box<dyn ITokenSeparator> {
    ru::build()
}

#[test]
fn test_text() {
    let text = r#"
        Это что, Америка, блядь? Вы что делаете, блядь?
    "#;
    let tokens = separator().separate(text);
    let expected = &[
        Token::StartOfSentence,
        Token::Word("Это".to_string()),
        Token::Word("что".to_string()),
        Token::Punctuation(','),
        Token::Word("Америка".to_string()),
        Token::Punctuation(','),
        Token::Word("блядь".to_string()),
        Token::Punctuation('?'),
        Token::EndOfSentence,
        Token::StartOfSentence,
        Token::Word("Вы".to_string()),
        Token::Word("что".to_string()),
        Token::Word("делаете".to_string()),
        Token::Punctuation(','),
        Token::Word("блядь".to_string()),
        Token::Punctuation('?'),
        Token::EndOfSentence,
        Token::EndOfBlock,
    ];
    assert_eq!(tokens, expected);
}

#[test]
fn test_quotes() {
    let text = "Кто “мы”? Кто «мы»? \"Я\" тут один!";
    let tokens = separator().separate(text);
    let expected = &[
        Token::StartOfSentence,
        Token::Word("Кто".to_string()),
        Token::Punctuation('«'),
        Token::Word("мы".to_string()),
        Token::Punctuation('»'),
        Token::Punctuation('?'),
        Token::EndOfSentence,
        Token::StartOfSentence,
        Token::Word("Кто".to_string()),
        Token::Punctuation('«'),
        Token::Word("мы".to_string()),
        Token::Punctuation('»'),
        Token::Punctuation('?'),
        Token::EndOfSentence,
        Token::StartOfSentence,
        Token::Punctuation('«'),
        Token::Word("Я".to_string()),
        Token::Punctuation('»'),
        Token::Word("тут".to_string()),
        Token::Word("один".to_string()),
        Token::Punctuation('!'),
        Token::EndOfSentence,
        Token::EndOfBlock,
    ];
    assert_eq!(tokens, expected);
}
