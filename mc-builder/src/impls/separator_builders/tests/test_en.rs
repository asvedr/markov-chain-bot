use mc_base::entities::tokens::Token;

use crate::impls::separator_builders::en;
use crate::proto::prepare::ITokenSeparator;

fn separator() -> Box<dyn ITokenSeparator> {
    en::build()
}

#[test]
fn test_text() {
    let text = r#"
        How long is a fish...

        I'm confused
    "#;
    let tokens = separator().separate(text);
    let expected = &[
        Token::StartOfSentence,
        Token::Word("How".to_string()),
        Token::Word("long".to_string()),
        Token::Word("is".to_string()),
        Token::Word("a".to_string()),
        Token::Word("fish".to_string()),
        Token::Punctuation('…'),
        Token::EndOfSentence,
        Token::EndOfBlock,
        Token::StartOfSentence,
        Token::Word("I".to_string()),
        Token::Punctuation('\''),
        Token::Word("m".to_string()),
        Token::Word("confused".to_string()),
        Token::EndOfSentence,
        Token::EndOfBlock,
    ];
    assert_eq!(tokens, expected);
}

#[test]
fn test_process_quotes() {
    let text = "How many \"fish\"?";
    let tokens = separator().separate(text);
    let expected = &[
        Token::StartOfSentence,
        Token::Word("How".to_string()),
        Token::Word("many".to_string()),
        Token::Punctuation('«'),
        Token::Word("fish".to_string()),
        Token::Punctuation('»'),
        Token::Punctuation('?'),
        Token::EndOfSentence,
        Token::EndOfBlock,
    ];
    assert_eq!(tokens, expected);
}
