fn get_close_symbol(sym: char, open_to_close: &[(char, char)]) -> Option<char> {
    for (open, close) in open_to_close {
        if *open == sym {
            return Some(*close);
        }
    }
    None
}

pub fn countable_symbols_matches(text: &str, open_to_close: &[(char, char)]) -> bool {
    let mut expected = Vec::new();
    for sym in text.chars() {
        if expected.last() == Some(&sym) {
            expected.pop();
            continue;
        }
        if let Some(cl) = get_close_symbol(sym, open_to_close) {
            expected.push(cl);
        }
    }
    expected.is_empty()
}
