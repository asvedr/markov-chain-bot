use mc_base::entities::common::Lang;
use mc_base::entities::constants::SYMBOLS_TO_REPLACE_ON_PREPARE;
use mc_base::entities::lang_specific;
use mddd::macros::singleton_simple;

use crate::impls::separator_builders::common::countable_symbols_matches;
use crate::impls::separators::{block_separator, sentence_separator, text_separator};
use crate::proto::prepare::ITokenSeparator;

const OPEN_TO_CLOSE: &[(char, char)] =
    &[('"', '"'), ('“', '”'), ('«', '»'), ('(', ')'), ('[', ']')];
const QUOTES: &[(char, char)] = &[('"', '"'), ('“', '”'), ('«', '»')];

fn quotes_not_matched(sen: &str) -> bool {
    !countable_symbols_matches(sen, OPEN_TO_CLOSE)
}

singleton_simple!(
    fn symbols_to_replace() -> Vec<(&'static str, &'static str)> {
        let words_to_replace = lang_specific::get(Lang::Ru).word_replacements_parsetime();
        SYMBOLS_TO_REPLACE_ON_PREPARE
            .iter()
            .chain(words_to_replace.iter())
            .cloned()
            .collect::<Vec<(&str, &str)>>()
    }
);

pub fn build() -> Box<dyn ITokenSeparator> {
    let lang = lang_specific::get(Lang::Ru);
    let sep = text_separator::Separator::new(block_separator::Separator::new(
        sentence_separator::Separator::new(lang.alphabet(), lang.acceptable_punctuation()),
        vec![&quotes_not_matched],
        symbols_to_replace(),
        QUOTES,
    ));
    Box::new(sep)
}
