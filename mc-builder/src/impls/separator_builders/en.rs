use mc_base::entities::common::Lang;
use mc_base::entities::constants::SYMBOLS_TO_REPLACE_ON_PREPARE;
use mc_base::entities::lang_specific;

use crate::impls::separator_builders::common::countable_symbols_matches;
use crate::impls::separators::{block_separator, sentence_separator, text_separator};
use crate::proto::prepare::ITokenSeparator;

const OPEN_TO_CLOSE: &[(char, char)] = &[('"', '"'), ('(', ')'), ('[', ']')];
const QUOTES: &[(char, char)] = &[('"', '"')];

fn quotes_not_matched(sen: &str) -> bool {
    !countable_symbols_matches(sen, OPEN_TO_CLOSE)
}

pub fn build() -> Box<dyn ITokenSeparator> {
    let lang = lang_specific::get(Lang::En);
    let sep = text_separator::Separator::new(block_separator::Separator::new(
        sentence_separator::Separator::new(lang.alphabet(), lang.acceptable_punctuation()),
        vec![&quotes_not_matched],
        SYMBOLS_TO_REPLACE_ON_PREPARE,
        QUOTES,
    ));
    Box::new(sep)
}
