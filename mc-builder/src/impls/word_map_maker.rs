use std::collections::HashMap;

use mc_base::entities::words::WordType;

use crate::proto::process::IWordMapMaker;

pub struct WordMapMaker;

fn get_type(src: &str) -> WordType {
    if src.is_empty() {
        return WordType::Common;
    }
    let mut chars = src.chars();
    let first_upper = chars.next().unwrap().is_uppercase();
    if !first_upper {
        return WordType::Common;
    }
    if chars.all(|s| s.is_uppercase()) {
        WordType::FullCaps
    } else {
        WordType::FirstCaps
    }
}

impl IWordMapMaker for WordMapMaker {
    fn make_word_map(&self, words: &[&String]) -> HashMap<String, WordType> {
        let mut result: HashMap<String, WordType> = HashMap::with_capacity(words.len());
        for word in words {
            let lower = word.to_lowercase();
            let tp = get_type(word);
            match result.get(&lower) {
                Some(val) if val.as_int() <= tp.as_int() => continue,
                _ => (),
            }
            result.insert(lower, tp);
        }
        result
    }
}
