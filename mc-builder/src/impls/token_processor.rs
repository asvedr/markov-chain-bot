use std::ops::Range;

use easy_sqlite::errors::DbResult;
use mc_base::entities::chain::NextTokenTransition;
use mc_base::entities::tag::Tag;
use mc_base::entities::tokens::Token;
use mc_base::proto::db::{INextTokenRepo, ITagRepo};
use mc_base::utils::seq::chunkify;

use crate::entities::config::Config;
use crate::proto::process::{IChainBuilder, ITokenProcessor, TokenProcessorRequest};

pub struct TokenProcessor<NTR: INextTokenRepo, TR: ITagRepo, CB: IChainBuilder> {
    next_token_repo: NTR,
    tag_repo: TR,
    chain_builder: CB,
    tag_repo_bulk_size: usize,
    start_token: String,
}

impl<NTR: INextTokenRepo, TR: ITagRepo, CB: IChainBuilder> TokenProcessor<NTR, TR, CB> {
    pub fn new(config: &Config, next_token_repo: NTR, tag_repo: TR, chain_builder: CB) -> Self {
        Self {
            next_token_repo,
            tag_repo,
            chain_builder,
            tag_repo_bulk_size: config.tag_repo_bulk_size,
            start_token: format!("{}", Token::StartOfSentence),
        }
    }

    fn process_only_from_begin(
        &self,
        sentences: &[&[(Token, Vec<Tag>)]],
        rng: Range<usize>,
    ) -> DbResult<()> {
        for seq_len in rng {
            let chains = self
                .chain_builder
                .build_begins_with_tags(sentences, seq_len);
            self.save_chains_with_tags(&chains)?;
        }
        Ok(())
    }

    fn process_tagged(
        &self,
        tokens: &[(Token, Vec<Tag>)],
        only_from_begin: Range<usize>,
        rng: Range<usize>,
    ) -> DbResult<()> {
        for block in tokens.split_inclusive(|(t, _)| matches!(t, Token::EndOfBlock)) {
            for seq_len in rng.clone() {
                let mut chains = self.chain_builder.build_with_tags(block, seq_len);
                if only_from_begin.contains(&seq_len) {
                    chains = chains
                        .into_iter()
                        .filter(|(tr, _)| !self.is_start_of_sentence(tr))
                        .collect()
                }
                self.save_chains_with_tags(&chains)?
            }
        }
        Ok(())
    }

    fn process_not_tagged(
        &self,
        tokens: &[(Token, Vec<Tag>)],
        only_from_begin: Range<usize>,
        with_tags: Range<usize>,
        rng: Range<usize>,
    ) -> DbResult<()> {
        for block in tokens.split_inclusive(|(t, _)| matches!(t, Token::EndOfBlock)) {
            for seq_len in rng.clone() {
                if with_tags.contains(&seq_len) {
                    continue;
                }
                let mut chains = self.chain_builder.build_without_tags(block, seq_len);
                if only_from_begin.contains(&seq_len) {
                    chains = chains
                        .into_iter()
                        .filter(|tr| !self.is_start_of_sentence(tr))
                        .collect()
                }
                self.save_chains(&chains)?
            }
        }
        Ok(())
    }

    #[inline]
    fn save_chains(&self, ntt: &[NextTokenTransition]) -> DbResult<()> {
        self.next_token_repo.inc_transitions(ntt)?;
        Ok(())
    }

    #[inline]
    fn save_chains_with_tags(&self, chains: &[(NextTokenTransition, Vec<Tag>)]) -> DbResult<()> {
        let map = self.next_token_repo.inc_transitions(chains)?;
        let mut to_save = vec![];
        for (tr, tags) in chains {
            let id = *map.get(&tr.key()).unwrap();
            for tag in tags {
                to_save.push((tag.clone(), id));
            }
        }
        for chunk in chunkify(&to_save, self.tag_repo_bulk_size) {
            self.tag_repo.save_tags(chunk)?;
        }
        Ok(())
    }

    #[inline]
    fn is_start_of_sentence(&self, tr: &NextTokenTransition) -> bool {
        tr.seq_tokens.starts_with(&self.start_token)
    }

    #[inline]
    fn recollect_as_sentences(tokens: &[(Token, Vec<Tag>)]) -> Vec<&[(Token, Vec<Tag>)]> {
        let mut result = Vec::new();
        for sentence in tokens.split_inclusive(|(t, _)| matches!(t, Token::EndOfSentence)) {
            if !matches!(sentence[0], (Token::StartOfSentence, _)) {
                continue;
            }
            result.push(sentence);
        }
        result
    }
}

impl<NTR: INextTokenRepo, TR: ITagRepo, CB: IChainBuilder> ITokenProcessor
    for TokenProcessor<NTR, TR, CB>
{
    fn process_tokens(&self, request: TokenProcessorRequest) -> DbResult<()> {
        let as_sentences = Self::recollect_as_sentences(request.tokens);
        self.process_only_from_begin(&as_sentences, request.only_from_begin.clone())?;
        self.process_tagged(
            request.tokens,
            request.only_from_begin.clone(),
            request.with_tags.clone(),
        )?;
        self.process_not_tagged(
            request.tokens,
            request.only_from_begin,
            request.with_tags,
            request.without_tags,
        )
    }
}
