use mc_base::entities::constants::{SENTENCE_TERMINATORS, UNI_QUOTE_CLOSE, UNI_QUOTE_OPEN};
use mc_base::entities::tokens::Token;

use crate::proto::prepare::ITokenSeparator;

pub struct Separator<SS: ITokenSeparator> {
    sentence_separator: SS,
    sentence_filters: Vec<&'static dyn Fn(&str) -> bool>,
    symbols_to_replace: &'static [(&'static str, &'static str)],
    quotes: &'static [(char, char)],
}

fn replace_all(src: &str, patts: &[(&str, &str)]) -> String {
    assert!(!patts.is_empty());
    let (from, to) = patts[0];
    let replaced = src.replace(from, to);
    if patts.len() == 1 {
        return replaced;
    }
    replace_all(&replaced, &patts[1..])
}

impl<SS: ITokenSeparator> Separator<SS> {
    pub fn new(
        sentence_separator: SS,
        sentence_filters: Vec<&'static dyn Fn(&str) -> bool>,
        symbols_to_replace: &'static [(&'static str, &'static str)],
        quotes: &'static [(char, char)],
    ) -> Self {
        Self {
            sentence_separator,
            sentence_filters,
            symbols_to_replace,
            quotes,
        }
    }

    fn replace_quotes(&self, tokens: Vec<Token>) -> Vec<Token> {
        let mut result = Vec::with_capacity(tokens.len());
        let mut close_expected = Vec::new();
        for token in tokens {
            let sym = match token {
                Token::Punctuation(val) => val,
                other => {
                    result.push(other);
                    continue;
                }
            };
            if close_expected.last() == Some(&sym) {
                close_expected.pop();
                result.push(Token::Punctuation(UNI_QUOTE_CLOSE));
                continue;
            }
            if let Some(exp) = self.find_quote_close_to_open(sym) {
                result.push(Token::Punctuation(UNI_QUOTE_OPEN));
                close_expected.push(exp);
            } else {
                result.push(Token::Punctuation(sym))
            }
        }
        result
    }

    fn find_quote_close_to_open(&self, sym: char) -> Option<char> {
        for (open, close) in self.quotes {
            if *open == sym {
                return Some(*close);
            }
        }
        None
    }

    #[inline]
    fn ignore_sentence(&self, sen: &str) -> bool {
        for filter in self.sentence_filters.iter() {
            if filter(sen) {
                return true;
            }
        }
        false
    }

    #[inline]
    fn ignore_tokens(&self, tokens: &[Token]) -> bool {
        tokens.is_empty()
            || tokens
                .iter()
                .all(|tkn| matches!(tkn, Token::Punctuation(_)))
    }
}

impl<SS: ITokenSeparator> ITokenSeparator for Separator<SS> {
    fn separate(&self, src: &str) -> Vec<Token> {
        let text = replace_all(src, self.symbols_to_replace);
        let mut result = Vec::new();
        for sentence in text.split_inclusive(|sym| SENTENCE_TERMINATORS.contains(sym)) {
            if self.ignore_sentence(sentence) {
                continue;
            }
            let tokens = self.sentence_separator.separate(sentence);
            if self.ignore_tokens(&tokens) {
                continue;
            }
            result.push(Token::StartOfSentence);
            result.append(&mut self.replace_quotes(tokens));
            result.push(Token::EndOfSentence);
        }
        result
    }
}
