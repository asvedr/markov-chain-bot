use mc_base::entities::tokens::Token;

use crate::proto::prepare::ITokenSeparator;

pub struct Separator<BS: ITokenSeparator> {
    block_separator: BS,
}

impl<BS: ITokenSeparator> Separator<BS> {
    pub fn new(block_separator: BS) -> Self {
        Self { block_separator }
    }
}

impl<BS: ITokenSeparator> ITokenSeparator for Separator<BS> {
    fn separate(&self, src: &str) -> Vec<Token> {
        let mut result = Vec::new();
        for block in src.split('\n') {
            let trimmed = block.trim();
            if trimmed.is_empty() {
                continue;
            }
            let mut tokens = self.block_separator.separate(block);
            if tokens.is_empty() {
                continue;
            }
            result.append(&mut tokens);
            result.push(Token::EndOfBlock);
        }
        result
    }
}
