use std::collections::HashSet;
use std::mem;

use mc_base::entities::constants::{SHARED_PUNCTUATION, SPACE};
use mc_base::entities::tokens::Token;

use crate::proto::prepare::ITokenSeparator;

pub struct Separator {
    word_symbols: HashSet<char>,
    punctuation: HashSet<char>,
}

#[inline]
fn push_word(tokens: &mut Vec<Token>, word: &mut String) {
    if word.is_empty() {
        return;
    }
    tokens.push(Token::Word(mem::take(word)));
}

impl Separator {
    pub fn new(word_symbols: impl ToString, punctuation: &str) -> Self {
        Self {
            word_symbols: word_symbols.to_string().chars().collect(),
            punctuation: SHARED_PUNCTUATION
                .to_string()
                .chars()
                .chain(punctuation.chars())
                .collect(),
        }
    }
}

impl ITokenSeparator for Separator {
    fn separate(&self, src: &str) -> Vec<Token> {
        let mut result = Vec::new();
        let mut word = String::new();
        for sym in src.chars() {
            if sym == SPACE {
                push_word(&mut result, &mut word);
            } else if self.word_symbols.contains(&sym) {
                word.push(sym);
            } else if self.punctuation.contains(&sym) {
                push_word(&mut result, &mut word);
                result.push(Token::Punctuation(sym));
            } else {
                return Vec::new();
            }
        }
        push_word(&mut result, &mut word);
        result
    }
}
