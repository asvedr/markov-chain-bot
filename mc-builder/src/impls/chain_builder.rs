use std::collections::{HashMap, HashSet};

use mc_base::entities::chain::NextTokenTransition;
use mc_base::entities::tag::Tag;
use mc_base::entities::tokens::Token;

use crate::proto::process::IChainBuilder;

pub struct ChainBuilder;

fn inc_tags(
    result: &mut HashMap<String, (NextTokenTransition, HashSet<Tag>)>,
    tr: NextTokenTransition,
    new_tags: &[Tag],
) {
    let key = tr.key();
    let cloned_tags = new_tags.iter().cloned();
    if let Some((tr, tags)) = result.get_mut(&key) {
        tr.count += 1;
        tags.extend(cloned_tags);
        return;
    }
    result.insert(key, (tr, cloned_tags.collect()));
}

fn inc_pure(result: &mut HashMap<String, NextTokenTransition>, tr: NextTokenTransition) {
    let key = tr.key();
    if let Some(tr) = result.get_mut(&key) {
        tr.count += 1;
        return;
    }
    result.insert(key, tr);
}

fn make_transition(
    tokens: &[(Token, Vec<Tag>)],
    begin: usize,
    chain_len: usize,
) -> NextTokenTransition {
    use std::fmt::Write;

    let mut seq = format!("{}", &tokens[begin].0);
    for (token, _) in &tokens[begin + 1..begin + chain_len] {
        write!(seq, " {}", token).unwrap();
    }
    NextTokenTransition {
        seq_tokens: seq,
        next_token: tokens[begin + chain_len].0.clone(),
        count: 1,
    }
}

fn convert_result(
    map: HashMap<String, (NextTokenTransition, HashSet<Tag>)>,
) -> Vec<(NextTokenTransition, Vec<Tag>)> {
    map.into_values()
        .map(|(tr, tags)| (tr, tags.into_iter().collect::<Vec<_>>()))
        .collect()
}

fn get_sentence_tags(tokens: &[(Token, Vec<Tag>)]) -> &[Tag] {
    for (_, tags) in tokens {
        if !tags.is_empty() {
            return tags;
        }
    }
    &[]
}

impl IChainBuilder for ChainBuilder {
    fn build_begins_with_tags(
        &self,
        sentences: &[&[(Token, Vec<Tag>)]],
        chain_len: usize,
    ) -> Vec<(NextTokenTransition, Vec<Tag>)> {
        let mut result = HashMap::new();
        let full_seq_len = chain_len + 1;
        for sentence in sentences {
            if full_seq_len > sentence.len() {
                continue;
            }
            let tr = make_transition(sentence, 0, chain_len);
            // sentence 0 is StartOfSentence and it doesn't have any tags
            let tags = get_sentence_tags(sentence);
            // let (_, ref tags) = sentence[1];
            inc_tags(&mut result, tr, tags);
        }
        convert_result(result)
    }

    fn build_with_tags(
        &self,
        tokens: &[(Token, Vec<Tag>)],
        chain_len: usize,
    ) -> Vec<(NextTokenTransition, Vec<Tag>)> {
        let full_seq_len = chain_len + 1;
        if full_seq_len > tokens.len() {
            return Default::default();
        }
        let mut result = HashMap::new();
        for i in 0..=tokens.len() - full_seq_len {
            let tr = make_transition(tokens, i, chain_len);
            let tags = get_sentence_tags(&tokens[i..=i + chain_len]);
            inc_tags(&mut result, tr, tags);
        }
        convert_result(result)
    }

    fn build_without_tags(
        &self,
        tokens: &[(Token, Vec<Tag>)],
        chain_len: usize,
    ) -> Vec<NextTokenTransition> {
        let full_seq_len = chain_len + 1;
        if full_seq_len > tokens.len() {
            return Default::default();
        }
        let mut result = HashMap::new();
        for i in 0..=tokens.len() - full_seq_len {
            let tr = make_transition(tokens, i, chain_len);
            inc_pure(&mut result, tr);
        }
        result.into_values().collect()
    }
}
