use std::mem;

use mc_base::entities::constants::SPECIFIC_WORD_PREFIX;
use mc_base::entities::tag::{Tag, TagType};
use mc_base::entities::tokens::Token;
use mc_base::proto::common::IStemmer;

use crate::proto::prepare::{ITagDetector, ITagger};

pub struct Tagger<TD: ITagDetector + 'static, S: IStemmer> {
    pub detectors: &'static [TD],
    pub stemmer: S,
}

fn store_sentence(
    result: &mut Vec<(Token, Vec<Tag>)>,
    sentence: &mut Vec<Token>,
    get_tags: &impl Fn(&[Token]) -> Vec<Tag>,
) {
    let tags = get_tags(sentence);
    for token in mem::take(sentence) {
        result.push((token, tags.clone()))
    }
}

#[inline]
fn get_last_sentence_begin(tokens: &[(Token, Vec<Tag>)]) -> Option<usize> {
    for i in (0..tokens.len()).rev() {
        if matches!(tokens[i], (Token::StartOfSentence, _)) {
            return Some(i + 1);
        }
    }
    None
}

fn mark_last_sentence_as_eob(tokens: &mut Vec<(Token, Vec<Tag>)>) {
    let begin = match get_last_sentence_begin(tokens) {
        Some(val) => val,
        _ => return,
    };
    let tag = Tag {
        tag_type: TagType::LastSentenceInBlock,
        key: "".to_string(),
    };
    // ignore [tokens.len() - 1] because it's ::EndOfSentence
    for i in begin + 1..tokens.len() - 1 {
        tokens[i].1.push(tag.clone())
    }
    tokens[begin].1.push(tag)
}

fn go_through_tokens(
    tokens: Vec<Token>,
    get_tags: impl Fn(&[Token]) -> Vec<Tag>,
) -> Vec<(Token, Vec<Tag>)> {
    let mut result = Vec::new();
    let mut sentence = Vec::new();
    for token in tokens {
        match token {
            Token::StartOfSentence => {
                assert!(sentence.is_empty());
                result.push((Token::StartOfSentence, vec![]));
            }
            Token::EndOfSentence => {
                assert!(!sentence.is_empty());
                store_sentence(&mut result, &mut sentence, &get_tags);
                result.push((Token::EndOfSentence, vec![]));
            }
            Token::EndOfBlock => {
                assert!(sentence.is_empty());
                mark_last_sentence_as_eob(&mut result);
                result.push((Token::EndOfBlock, vec![]));
            }
            Token::Punctuation(val) => sentence.push(Token::Punctuation(val)),
            Token::Word(val) => sentence.push(Token::Word(val.to_lowercase())),
        }
    }
    assert!(sentence.is_empty());
    result
}

impl<TD: ITagDetector + 'static, S: IStemmer> Tagger<TD, S> {
    fn get_sentence_tags(&self, sentence: &[Token]) -> Vec<Tag> {
        let mut result = Vec::new();
        let stems = sentence
            .iter()
            .filter_map(|t| self.get_stem(t))
            .collect::<Vec<_>>();
        for detector in self.detectors.iter() {
            let key = match detector.detect(&stems) {
                Some(val) => val,
                _ => continue,
            };
            result.push(Tag {
                tag_type: detector.tag_type(),
                key: key.to_string(),
            });
        }
        for key in stems {
            if !key.starts_with(SPECIFIC_WORD_PREFIX) {
                let tag_type = TagType::Word;
                result.push(Tag { tag_type, key });
            }
        }
        result
    }

    fn get_stem(&self, token: &Token) -> Option<String> {
        let word = match token {
            Token::Word(ref val) => val,
            _ => return None,
        };
        Some(self.stemmer.stem(word).to_string())
    }
}

// Expected seq type <StartOfSentence>, <...>, <EndOfSentence>, <EndOfBlock>,
impl<TD: ITagDetector + 'static, S: IStemmer> ITagger for Tagger<TD, S> {
    fn reveal_tags(&self, tokens: Vec<Token>, tags: Vec<Tag>) -> Vec<(Token, Vec<Tag>)> {
        if tags.is_empty() {
            let tokenize = |sentence: &[Token]| self.get_sentence_tags(sentence);
            go_through_tokens(tokens, tokenize)
        } else {
            let tokenize = |sentence: &[Token]| {
                let mut got = self.get_sentence_tags(sentence);
                got.append(&mut tags.clone());
                got
            };
            go_through_tokens(tokens, tokenize)
        }
    }
}
