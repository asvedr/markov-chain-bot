use easy_sqlite::errors::DbResult;
use easy_sqlite::{IExecutor, RSQLConnection};
use mc_base::entities::tag::{Tag, TagType};
use mc_base::entities::tokens::Token;
use mc_base::impls::sqlite::next_token_repo::NextTokenRepo;
use mc_base::impls::sqlite::tag_repo::TagRepo;
use mddd::traits::IConfig;
use rusqlite::{Row, ToSql};

use crate::entities::config::Config;
use crate::impls::chain_builder::ChainBuilder;
use crate::impls::token_processor::TokenProcessor;
use crate::proto::process::{ITokenProcessor, TokenProcessorRequest};

struct MockedProcessor {
    pub cnn: RSQLConnection,
    pub processor:
        TokenProcessor<NextTokenRepo<RSQLConnection>, TagRepo<RSQLConnection>, ChainBuilder>,
}

impl ITokenProcessor for MockedProcessor {
    fn process_tokens(&self, request: TokenProcessorRequest) -> DbResult<()> {
        self.processor.process_tokens(request)
    }
}

impl IExecutor for MockedProcessor {
    fn get_one<T, F: FnMut(&Row<'_>) -> rusqlite::Result<T>>(
        &self,
        query: &str,
        params: &[&dyn ToSql],
        serializer: F,
    ) -> DbResult<T> {
        self.cnn.get_one(query, params, serializer)
    }

    fn get_many<T, F: FnMut(&Row<'_>) -> rusqlite::Result<T>>(
        &self,
        query: &str,
        params: &[&dyn ToSql],
        serializer: F,
    ) -> DbResult<Vec<T>> {
        self.cnn.get_many(query, params, serializer)
    }

    fn execute(&self, query: &str, params: &[&dyn ToSql]) -> DbResult<()> {
        self.cnn.execute(query, params)
    }

    fn execute_return_id(&self, query: &str, params: &[&dyn ToSql]) -> DbResult<i64> {
        self.cnn.execute_return_id(query, params)
    }
}

fn make_processor() -> MockedProcessor {
    let cnn = RSQLConnection::new(":memory:").unwrap();
    let processor = TokenProcessor::new(
        &Config::build().unwrap(),
        NextTokenRepo::new(cnn.clone()).unwrap(),
        TagRepo::new(cnn.clone()).unwrap(),
        ChainBuilder,
    );
    MockedProcessor { cnn, processor }
}

fn tags1() -> Vec<Tag> {
    vec![Tag {
        tag_type: TagType::Word,
        key: "a".to_string(),
    }]
}

fn tags2() -> Vec<Tag> {
    vec![Tag {
        tag_type: TagType::Word,
        key: "b".to_string(),
    }]
}

fn tokens() -> Vec<(Token, Vec<Tag>)> {
    vec![
        (Token::StartOfSentence, tags1()),
        (Token::Word("hi".to_string()), tags1()),
        (Token::Punctuation(','), tags1()),
        (Token::Word("how".to_string()), tags1()),
        (Token::EndOfSentence, tags1()),
        (Token::StartOfSentence, tags2()),
        (Token::Word("a".to_string()), tags2()),
        (Token::Word("b".to_string()), tags2()),
        (Token::EndOfSentence, tags2()),
        (Token::EndOfBlock, vec![]),
    ]
}

#[test]
fn test_process_tokens_tags() {
    let proc = make_processor();
    let mut expected = vec![
        ("s>whi", Some("na")),
        ("s>wa", Some("nb")),
        ("s whi>p,", Some("na")),
        ("s wa>wb", Some("nb")),
        ("s whi p,>whow", Some("na")),
        ("whi p, whow>e", Some("na")),
        ("p, whow e>s", Some("na")),
        ("whow e s>wa", Some("na")),
        ("e s wa>wb", Some("na")),
        ("s wa wb>e", Some("nb")),
        ("wa wb e>E", Some("nb")),
    ];
    expected.sort_by_key(|(seq, _)| *seq);
    proc.process_tokens(TokenProcessorRequest {
        tokens: &tokens(),
        only_from_begin: (1..3),
        with_tags: (3..4),
        ..Default::default()
    })
    .unwrap();

    let query = r#"
        SELECT nt.slug, t.key
            FROM next_token as nt LEFT JOIN tag as t
                ON t.next_token_id = nt.id
        ORDER BY nt.slug
    "#;
    let rows: Vec<(String, Option<String>)> = proc
        .get_many(&query, &[], |r| Ok((r.get(0)?, r.get(1)?)))
        .unwrap();
    let res = rows
        .iter()
        .map(|(slug, key)| -> (&str, Option<&str>) { (&slug, key.as_ref().map(|k| -> &str { k })) })
        .collect::<Vec<_>>();
    assert_eq!(res, expected)
}

#[test]
fn test_process_token_no_tags() {
    let proc = make_processor();
    let mut expected = vec![
        ("s>whi", Some("na")),
        ("s>wa", Some("nb")),
        ("s whi>p,", Some("na")),
        ("s wa>wb", Some("nb")),
        ("s whi p,>whow", None),
        ("whi p, whow>e", None),
        ("p, whow e>s", None),
        ("whow e s>wa", None),
        ("e s wa>wb", None),
        ("s wa wb>e", None),
        ("wa wb e>E", None),
    ];
    expected.sort_by_key(|(seq, _)| *seq);
    proc.process_tokens(TokenProcessorRequest {
        tokens: &tokens(),
        only_from_begin: (1..3),
        without_tags: (3..4),
        ..Default::default()
    })
    .unwrap();

    let query = r#"
        SELECT nt.slug, t.key
            FROM next_token as nt LEFT JOIN tag as t
                ON t.next_token_id = nt.id
        ORDER BY nt.slug
    "#;
    let rows: Vec<(String, Option<String>)> = proc
        .get_many(&query, &[], |r| Ok((r.get(0)?, r.get(1)?)))
        .unwrap();
    let res = rows
        .iter()
        .map(|(slug, key)| -> (&str, Option<&str>) { (&slug, key.as_ref().map(|k| -> &str { k })) })
        .collect::<Vec<_>>();
    assert_eq!(res, expected)
}

#[test]
fn test_process_2_and_2() {
    let proc = make_processor();
    proc.process_tokens(TokenProcessorRequest {
        tokens: &tokens(),
        only_from_begin: (1..3),
        without_tags: (2..3),
        ..Default::default()
    })
    .unwrap();
    let query = r#"
        SELECT nt.slug, t.key
            FROM next_token as nt LEFT JOIN tag as t
                ON t.next_token_id = nt.id
        ORDER BY nt.slug
    "#;
    let rows: Vec<(String, Option<String>)> = proc
        .get_many(&query, &[], |r| Ok((r.get(0)?, r.get(1)?)))
        .unwrap();
    let res = rows
        .iter()
        .map(|(slug, key)| -> (&str, Option<&str>) { (&slug, key.as_ref().map(|k| -> &str { k })) })
        .collect::<Vec<_>>();
    let expected = &[
        ("e s>wa", None),
        ("p, whow>e", None),
        ("s wa>wb", Some("nb")),
        ("s whi>p,", Some("na")),
        ("s>wa", Some("nb")),
        ("s>whi", Some("na")),
        ("wa wb>e", None),
        ("wb e>E", None),
        ("whi p,>whow", None),
        ("whow e>s", None),
    ];
    assert_eq!(res, expected)
}
