use mc_base::entities::chain::NextTokenTransition;
use mc_base::entities::tag::{Tag, TagType};
use mc_base::entities::tokens::Token;

use crate::impls::chain_builder::ChainBuilder;
use crate::proto::process::IChainBuilder;

struct TBuilder {
    inner: ChainBuilder,
}

fn sort_key(ntt: &NextTokenTransition) -> String {
    ntt.key()
}

impl IChainBuilder for TBuilder {
    fn build_begins_with_tags(
        &self,
        tokens: &[&[(Token, Vec<Tag>)]],
        chain_len: usize,
    ) -> Vec<(NextTokenTransition, Vec<Tag>)> {
        let mut res = self.inner.build_begins_with_tags(tokens, chain_len);
        res.sort_by_key(|(tr, _)| sort_key(tr));
        for (_, tags) in res.iter_mut() {
            tags.sort()
        }
        res
    }

    fn build_with_tags(
        &self,
        tokens: &[(Token, Vec<Tag>)],
        chain_len: usize,
    ) -> Vec<(NextTokenTransition, Vec<Tag>)> {
        let mut res = self.inner.build_with_tags(tokens, chain_len);
        res.sort_by_key(|(tr, _)| sort_key(tr));
        for (_, tags) in res.iter_mut() {
            tags.sort()
        }
        res
    }

    fn build_without_tags(
        &self,
        tokens: &[(Token, Vec<Tag>)],
        chain_len: usize,
    ) -> Vec<NextTokenTransition> {
        let mut res = self.inner.build_without_tags(tokens, chain_len);
        res.sort_by_key(sort_key);
        res
    }
}

fn builder() -> TBuilder {
    TBuilder {
        inner: ChainBuilder,
    }
}

fn tokens() -> Vec<(Token, Vec<Tag>)> {
    let tags1 = vec![Tag {
        tag_type: TagType::QBad,
        key: Default::default(),
    }];
    let tags2 = vec![Tag {
        tag_type: TagType::Word,
        key: "a".to_string(),
    }];
    vec![
        (Token::StartOfSentence, tags1.clone()),
        (Token::Word("hi".to_string()), tags1.clone()),
        (Token::Word("it".to_string()), tags1.clone()),
        (Token::Punctuation('\''), tags1.clone()),
        (Token::Word("s".to_string()), tags1.clone()),
        (Token::EndOfSentence, tags1),
        (Token::StartOfSentence, tags2.clone()),
        (Token::Word("it".to_string()), tags2.clone()),
        (Token::Punctuation('\''), tags2.clone()),
        (Token::Word("s".to_string()), tags2.clone()),
        (Token::EndOfSentence, tags2),
        (Token::EndOfBlock, vec![]),
    ]
}

fn expected() -> Vec<(NextTokenTransition, Vec<Tag>)> {
    let tags1 = vec![Tag {
        tag_type: TagType::QBad,
        key: Default::default(),
    }];
    let tags2 = vec![Tag {
        tag_type: TagType::Word,
        key: "a".to_string(),
    }];
    let mut tags1p2 = tags1.clone();
    tags1p2.extend(tags2.clone());
    let mut res = vec![
        (
            NextTokenTransition {
                seq_tokens: "s whi wit".to_string(),
                next_token: Token::Punctuation('\''),
                count: 1,
            },
            tags1.clone(),
        ),
        (
            NextTokenTransition {
                seq_tokens: "whi wit p'".to_string(),
                next_token: Token::Word("s".to_string()),
                count: 1,
            },
            tags1.clone(),
        ),
        (
            NextTokenTransition {
                seq_tokens: "wit p' ws".to_string(),
                next_token: Token::EndOfSentence,
                count: 2,
            },
            tags1p2,
        ),
        (
            NextTokenTransition {
                seq_tokens: "p' ws e".to_string(),
                next_token: Token::StartOfSentence,
                count: 1,
            },
            tags1.clone(),
        ),
        (
            NextTokenTransition {
                seq_tokens: "ws e s".to_string(),
                next_token: Token::Word("it".to_string()),
                count: 1,
            },
            tags1.clone(),
        ),
        (
            NextTokenTransition {
                seq_tokens: "e s wit".to_string(),
                next_token: Token::Punctuation('\''),
                count: 1,
            },
            tags1,
        ),
        (
            NextTokenTransition {
                seq_tokens: "s wit p'".to_string(),
                next_token: Token::Word("s".to_string()),
                count: 1,
            },
            tags2.clone(),
        ),
        (
            NextTokenTransition {
                seq_tokens: "p' ws e".to_string(),
                next_token: Token::EndOfBlock,
                count: 1,
            },
            tags2,
        ),
    ];
    res.sort_by_key(|(tr, _)| sort_key(tr));
    for (_, tags) in res.iter_mut() {
        tags.sort()
    }
    res
}

#[test]
fn test_build_chain_too_small_no_tags() {
    assert!(builder().build_without_tags(&[], 3).is_empty())
}

#[test]
fn test_build_chain_too_small_tags() {
    assert!(builder().build_with_tags(&[], 3).is_empty())
}

#[test]
fn test_build_chain_no_tags() {
    let chain = builder().build_without_tags(&tokens(), 3);
    let expected = expected().into_iter().map(|(tr, _)| tr).collect::<Vec<_>>();
    assert_eq!(chain, expected);
}

#[test]
fn test_build_chain_tags() {
    let chain = builder().build_with_tags(&tokens(), 3);
    let exp = expected();
    assert_eq!(chain, exp);
}

#[test]
fn test_build_begins() {
    let tkns = tokens();
    let split = tkns
        .split_inclusive(|(tkn, _)| matches!(tkn, Token::EndOfSentence))
        .collect::<Vec<_>>();
    let chain = builder().build_begins_with_tags(&[&split[0], &split[1]], 3);
    let tags1 = vec![Tag {
        tag_type: TagType::QBad,
        key: Default::default(),
    }];
    let tags2 = vec![Tag {
        tag_type: TagType::Word,
        key: "a".to_string(),
    }];
    assert_eq!(
        chain,
        &[
            (
                NextTokenTransition {
                    seq_tokens: "s whi wit".to_string(),
                    next_token: Token::Punctuation('\''),
                    count: 1
                },
                tags1
            ),
            (
                NextTokenTransition {
                    seq_tokens: "s wit p'".to_string(),
                    next_token: Token::Word("s".to_string()),
                    count: 1
                },
                tags2
            ),
        ]
    )
}

#[test]
fn test_get_tags_even_if_first_not_contains() {
    let tag1 = vec![Tag {
        tag_type: TagType::QNeed,
        key: "".to_string(),
    }];
    let tag2 = vec![Tag {
        tag_type: TagType::QBad,
        key: "".to_string(),
    }];
    let tokens = &[
        (Token::Punctuation('.'), tag1.clone()),
        (Token::EndOfSentence, vec![]),
        (Token::StartOfSentence, vec![]),
        (Token::Word("a".to_string()), tag2.clone()),
        (Token::EndOfBlock, vec![]),
    ];
    let chain = builder().build_with_tags(tokens, 3);
    let mut expected = vec![
        (
            NextTokenTransition {
                seq_tokens: "p. e s".to_string(),
                next_token: Token::Word("a".to_string()),
                count: 1,
            },
            tag1,
        ),
        (
            NextTokenTransition {
                seq_tokens: "e s wa".to_string(),
                next_token: Token::EndOfBlock,
                count: 1,
            },
            tag2,
        ),
    ];
    expected.sort_by_key(|(tr, _)| sort_key(tr));
    assert_eq!(chain, expected)
}
