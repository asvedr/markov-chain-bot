use easy_sqlite::RSQLConnection;

use mc_base::entities::tokens::Token;
use mc_base::entities::words::WordType;
use mc_base::impls::sqlite::word_repo::WordRepo;
use mc_base::proto::db::IWordRepo;

use crate::impls::word_map_maker::WordMapMaker;
use crate::impls::word_processor::WordProcessor;
use crate::proto::process::IWordProcessor;

type TRepo = WordRepo<RSQLConnection>;
type TWordProcessor = WordProcessor<TRepo, WordMapMaker>;

fn make_processor() -> (TRepo, TWordProcessor) {
    let cnn = RSQLConnection::new(":memory:").unwrap();
    let repo = WordRepo::new(cnn.clone()).unwrap();
    let proc = WordProcessor {
        repo: WordRepo::new(cnn).unwrap(),
        map_maker: WordMapMaker,
        chunk_size: 3,
    };
    (repo, proc)
}

#[test]
fn test_proc_tokens() {
    let tokens = &[
        Token::StartOfSentence,
        Token::Word("Это".to_string()),
        Token::Word("что".to_string()),
        Token::Punctuation(','),
        Token::Word("Америка".to_string()),
        Token::Punctuation(','),
        Token::Word("блядь".to_string()),
        Token::Word("это".to_string()),
        Token::Punctuation('?'),
        Token::EndOfSentence,
        Token::StartOfSentence,
        Token::Word("Вы".to_string()),
        Token::Word("что".to_string()),
        Token::Word("ДЕЛАЕТЕ".to_string()),
        Token::Punctuation(','),
        Token::Word("Блядь".to_string()),
        Token::Punctuation('?'),
        Token::EndOfSentence,
        Token::EndOfBlock,
    ];
    let (repo, proc) = make_processor();
    proc.update_words(tokens).unwrap();

    let words = &[
        ("это", WordType::Common),
        ("что", WordType::Common),
        ("америка", WordType::FirstCaps),
        ("блядь", WordType::Common),
        ("вы", WordType::FirstCaps),
        ("делаете", WordType::FullCaps),
    ];
    let words_s = words
        .iter()
        .map(|(w, tp)| (w.to_string(), *tp))
        .collect::<Vec<_>>();
    let keys = words_s.iter().map(|(w, _)| w).collect::<Vec<&String>>();
    let got = repo.get_words(&keys).unwrap();
    assert_eq!(got, words_s.into_iter().collect());
}
