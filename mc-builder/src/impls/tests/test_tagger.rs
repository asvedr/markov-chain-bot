use mc_base::entities::common::Lang;
use mc_base::entities::tag::{Tag, TagType};
use mc_base::entities::tokens::Token;
use mc_base::impls::stemmer::Stemmer;

use crate::impls::tag_detectors::ru;
use crate::impls::tagger::Tagger;
use crate::proto::prepare::ITagger;

fn make_tagger() -> Box<dyn ITagger> {
    let tagger = Tagger {
        detectors: ru::all(),
        stemmer: Stemmer::new(Lang::Ru),
    };
    Box::new(tagger)
}

fn stringify(src: Vec<(Token, Vec<Tag>)>) -> Vec<String> {
    src.into_iter()
        .map(|(tkn, mut tags)| {
            tags.sort();
            let tags = tags
                .into_iter()
                .map(|tag| format!("{}", tag))
                .collect::<Vec<_>>()
                .join(",");
            format!("{}|{}", tkn, tags)
        })
        .collect()
}

fn tokens() -> Vec<Token> {
    vec![
        Token::StartOfSentence,
        Token::Word("слон".to_string()),
        Token::Word("это".to_string()),
        Token::Punctuation(','),
        Token::Word("допустим".to_string()),
        Token::Punctuation('.'),
        Token::EndOfSentence,
        Token::StartOfSentence,
        Token::Word("куда".to_string()),
        Token::Word("ведет".to_string()),
        Token::EndOfSentence,
        Token::EndOfBlock,
    ]
}

#[test]
fn test_reveal() {
    let tagger = make_tagger();
    let tagged = stringify(tagger.reveal_tags(tokens(), vec![]));
    let expected = &[
        "s|",
        "wслон|nдопуст,nслон,wслон",
        "wэто|nдопуст,nслон,wслон",
        "p,|nдопуст,nслон,wслон",
        "wдопустим|nдопуст,nслон,wслон",
        "p.|nдопуст,nслон,wслон",
        "e|",
        "s|",
        "wкуда|nведет,nкуд,L",
        "wведет|nведет,nкуд,L",
        "e|",
        "E|",
    ];
    assert_eq!(tagged, expected)
}

#[test]
fn test_force() {
    let tagger = make_tagger();
    let tagged = stringify(tagger.reveal_tags(
        tokens(),
        vec![Tag {
            tag_type: TagType::QBad,
            key: "".to_string(),
        }],
    ));
    let expected = &[
        "s|",
        "wслон|nдопуст,nслон,wслон,b",
        "wэто|nдопуст,nслон,wслон,b",
        "p,|nдопуст,nслон,wслон,b",
        "wдопустим|nдопуст,nслон,wслон,b",
        "p.|nдопуст,nслон,wслон,b",
        "e|",
        "s|",
        "wкуда|nведет,nкуд,b,L",
        "wведет|nведет,nкуд,b,L",
        "e|",
        "E|",
    ];
    assert_eq!(tagged, expected)
}
