use crate::proto::prepare::ITagDetector;

pub mod need;
pub mod what_is;

pub fn all() -> &'static [&'static dyn ITagDetector] {
    const NEED: need::Detector = need::Detector;
    const WHAT_IS: what_is::Detector = what_is::Detector;

    const RESULT: &[&'static dyn ITagDetector] = &[&NEED, &WHAT_IS];
    RESULT
}
