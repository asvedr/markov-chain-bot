use mc_base::entities::lang_specific::ru::THIS_V;
use mc_base::entities::tag::TagType;

use crate::proto::prepare::ITagDetector;

pub struct Detector;

impl ITagDetector for Detector {
    fn tag_type(&self) -> TagType {
        TagType::QWhatIs
    }

    fn detect<'a>(&self, stems: &'a [String]) -> Option<&'a str> {
        for i in 1..stems.len() {
            if stems[i] == THIS_V {
                return Some(&stems[i - 1]);
            }
        }
        None
    }
}
