use mc_base::entities::tag::TagType;

use crate::proto::prepare::ITagDetector;

pub struct Detector;

// надо, нужно
const STEMS: &[&str] = &["над", "нужн"];

impl ITagDetector for Detector {
    fn tag_type(&self) -> TagType {
        TagType::QNeed
    }

    fn detect<'a>(&self, stems: &'a [String]) -> Option<&'a str> {
        let first_stem: &str = &stems[0];
        if STEMS.contains(&first_stem) {
            Some("")
        } else {
            None
        }
    }
}
