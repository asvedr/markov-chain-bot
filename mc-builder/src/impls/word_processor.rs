use easy_sqlite::errors::DbResult;

use mc_base::entities::tokens::Token;
use mc_base::proto::db::IWordRepo;
use mc_base::utils::seq::chunkify;

use crate::proto::process::{IWordMapMaker, IWordProcessor};

pub struct WordProcessor<WR: IWordRepo, WMM: IWordMapMaker> {
    pub repo: WR,
    pub map_maker: WMM,
    pub chunk_size: usize,
}

impl<WR: IWordRepo, WMM: IWordMapMaker> WordProcessor<WR, WMM> {
    fn update_chunk(&self, words: &[&String]) -> DbResult<()> {
        let new = self.map_maker.make_word_map(words);
        let keys = new.keys().collect::<Vec<_>>();
        let old = self.repo.get_words(&keys)?;
        let mut to_save = Vec::new();
        for (word, new_tp) in new.into_iter() {
            match old.get(&word) {
                Some(old_tp) if new_tp.as_int() >= old_tp.as_int() => continue,
                _ => (),
            }
            to_save.push((word, new_tp));
        }
        self.repo.save_words(&to_save)
    }
}

impl<WR: IWordRepo, WMM: IWordMapMaker> IWordProcessor for WordProcessor<WR, WMM> {
    fn update_words(&self, tokens: &[Token]) -> DbResult<()> {
        for chunk in chunkify(tokens, self.chunk_size) {
            let words = chunk
                .iter()
                .filter_map(|tkn| match tkn {
                    Token::Word(ref val) => Some(val),
                    _ => None,
                })
                .collect::<Vec<_>>();
            self.update_chunk(&words)?;
        }
        Ok(())
    }
}
