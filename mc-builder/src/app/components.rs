use minnixrand::urandom;

use mc_base::entities::lang_specific;
use mc_base::impls::generator::Generator;
use mc_base::impls::text_compiler::TextCompiler;
use mc_base::proto::gen::{IGenerator, ITextCompiler};

use crate::app::common::config;
use crate::app::db::{next_token_repo, settings_repo, tag_repo, word_repo};
use crate::impls::chain_builder::ChainBuilder;
use crate::impls::token_processor::TokenProcessor;
use crate::impls::word_map_maker::WordMapMaker;
use crate::impls::word_processor::WordProcessor;
use crate::proto::process::{ITokenProcessor, IWordProcessor};

pub fn token_processor(db_path: &str) -> Box<dyn ITokenProcessor> {
    let proc = TokenProcessor::new(
        config(),
        next_token_repo(db_path),
        tag_repo(db_path),
        ChainBuilder,
    );
    Box::new(proc)
}

pub fn word_processor(db_path: &str) -> Box<dyn IWordProcessor> {
    let proc = WordProcessor {
        repo: word_repo(db_path),
        map_maker: WordMapMaker,
        chunk_size: config().word_repo_bulk_size,
    };
    Box::new(proc)
}

pub fn text_compiler(db_path: &str) -> Box<dyn ITextCompiler> {
    use mc_base::proto::db::ISettingsRepo;

    let lang = settings_repo(db_path).get_lang().unwrap();
    let com = TextCompiler {
        word_repo: word_repo(db_path),
        chunk_size: config().text_compiler_bulk_size,
        consts: lang_specific::get(lang),
    };
    Box::new(com)
}

pub fn generator(db_path: &str) -> Box<dyn IGenerator> {
    let randomizer = match urandom::Randomizer::fast() {
        Ok(val) => val,
        Err(err) => panic!("Can not init randomizer: {:?}", err),
    };
    let gen = Generator::new(randomizer, next_token_repo(db_path));
    Box::new(gen)
}
