use mc_base::entities::common::Lang;
use mc_base::impls::stemmer::Stemmer;
use mddd::macros::singleton_simple;

use crate::app::stemmers::stemmer;
use crate::impls::tag_detectors::ru;
use crate::impls::tagger::Tagger;
use crate::proto::prepare::{ITagDetector, ITagger};

singleton_simple!(
    fn ru_tagger() -> Tagger<&'static dyn ITagDetector, Stemmer> {
        Tagger {
            detectors: ru::all(),
            stemmer: stemmer(Lang::Ru),
        }
    }
);

singleton_simple!(
    fn en_tagger() -> Tagger<&'static dyn ITagDetector, Stemmer> {
        const EMPTY: &[&dyn ITagDetector] = &[];
        Tagger {
            detectors: EMPTY,
            stemmer: stemmer(Lang::En),
        }
    }
);

pub fn tagger(lang: Lang) -> &'static dyn ITagger {
    match lang {
        Lang::Ru => ru_tagger(),
        Lang::En => en_tagger(),
    }
}
