use mc_base::entities::common::Lang;
use mc_base::impls::stemmer::Stemmer;

pub fn stemmer(lang: Lang) -> Stemmer {
    Stemmer::new(lang)
}
