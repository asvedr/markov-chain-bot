use easy_sqlite::RSQLConnection;

use mddd::macros::{singleton_mutex, singleton_simple};

use mc_base::impls::sqlite::next_token_repo::NextTokenRepo;
use mc_base::impls::sqlite::settings_repo::SettingsRepo;
use mc_base::impls::sqlite::tag_repo::TagRepo;
use mc_base::impls::sqlite::word_repo::WordRepo;

singleton_mutex!(
    fn connect(db_path: &str) -> RSQLConnection {
        match RSQLConnection::new(db_path) {
            Ok(val) => val,
            Err(err) => panic!("Can not connect db({}): {:?}", db_path, err),
        }
    }
);

singleton_simple!(
    pub fn word_repo(db_path: &str) -> WordRepo<&'static RSQLConnection> {
        match WordRepo::new(connect(db_path)) {
            Ok(val) => val,
            Err(err) => panic!("can not init word repo: {:?}", err),
        }
    }
);

singleton_simple!(
    pub fn next_token_repo(db_path: &str) -> NextTokenRepo<&'static RSQLConnection> {
        match NextTokenRepo::new(connect(db_path)) {
            Ok(val) => val,
            Err(err) => panic!("can not init NextTokenRepo: {:?}", err),
        }
    }
);

singleton_simple!(
    pub fn tag_repo(db_path: &str) -> TagRepo<&'static RSQLConnection> {
        match TagRepo::new(connect(db_path)) {
            Ok(val) => val,
            Err(err) => panic!("can not init TagRepo: {:?}", err),
        }
    }
);

singleton_simple!(
    pub fn settings_repo(db_path: &str) -> SettingsRepo<&'static RSQLConnection> {
        match SettingsRepo::new(connect(db_path)) {
            Ok(val) => val,
            Err(err) => panic!("can not init SettingsRepo: {:?}", err),
        }
    }
);
