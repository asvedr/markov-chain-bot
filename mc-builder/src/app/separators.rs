use mc_base::entities::common::Lang;
use mddd::macros::singleton_simple;

use crate::impls::separator_builders;
use crate::proto::prepare::ITokenSeparator;

singleton_simple!(
    fn ru() -> Box<dyn ITokenSeparator> {
        separator_builders::ru::build()
    }
);

singleton_simple!(
    fn en() -> Box<dyn ITokenSeparator> {
        separator_builders::en::build()
    }
);

pub fn get(lang: Lang) -> &'static dyn ITokenSeparator {
    let boxed = match lang {
        Lang::Ru => ru(),
        Lang::En => en(),
    };
    &**boxed
}
