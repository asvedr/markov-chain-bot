use mc_base::entities::common::Lang;
use mc_base::proto::common::IStemmer;
use mc_base::proto::db::ISettingsRepo;

use crate::app::{components, db, separators, stemmers, tag_detectors};
use crate::use_cases::generate::GenerateUC;
use crate::use_cases::process_text::ProcessTextUC;

fn settings_repo(db_path: &str) -> Box<dyn ISettingsRepo> {
    let repo = db::settings_repo(db_path);
    Box::new(repo)
}

pub fn process_text() -> ProcessTextUC {
    ProcessTextUC {
        separator_factory: &separators::get,
        tagger_factory: &tag_detectors::tagger,
        processor_factory: &components::token_processor,
        word_processor_factory: &components::word_processor,
        settings_repo_factory: &settings_repo,
    }
}

pub fn generate() -> GenerateUC {
    fn stemmer(lang: Lang) -> Box<dyn IStemmer> {
        Box::new(stemmers::stemmer(lang))
    }
    GenerateUC {
        generator_factory: &components::generator,
        compiler_factory: &components::text_compiler,
        settings_factory: &settings_repo,
        stemmer_factory: &stemmer,
    }
}
