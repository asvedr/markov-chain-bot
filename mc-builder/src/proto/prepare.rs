use mc_base::entities::tag::{Tag, TagType};
use mc_base::entities::tokens::Token;

pub trait ITokenSeparator {
    fn separate(&self, src: &str) -> Vec<Token>;
}

pub trait ITagger {
    fn reveal_tags(&self, tokens: Vec<Token>, tags: Vec<Tag>) -> Vec<(Token, Vec<Tag>)>;
}

pub trait ITagDetector {
    fn tag_type(&self) -> TagType;
    fn detect<'a>(&self, sentence: &'a [String]) -> Option<&'a str>;
}

// ------------------------
// -------- IMPLS ---------
// ------------------------

impl<T: ITokenSeparator> ITokenSeparator for &'_ T {
    fn separate(&self, src: &str) -> Vec<Token> {
        (**self).separate(src)
    }
}

impl<T: ITagger> ITagger for &'_ T {
    fn reveal_tags(&self, tokens: Vec<Token>, tags: Vec<Tag>) -> Vec<(Token, Vec<Tag>)> {
        (**self).reveal_tags(tokens, tags)
    }
}

impl<T: ITagDetector> ITagDetector for &'_ T {
    fn tag_type(&self) -> TagType {
        (**self).tag_type()
    }

    fn detect<'a>(&self, sentence: &'a [String]) -> Option<&'a str> {
        (**self).detect(sentence)
    }
}

impl ITagDetector for &'_ dyn ITagDetector {
    fn tag_type(&self) -> TagType {
        (**self).tag_type()
    }

    fn detect<'a>(&self, sentence: &'a [String]) -> Option<&'a str> {
        (**self).detect(sentence)
    }
}
