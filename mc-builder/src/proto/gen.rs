use crate::entities::errors::{GeneratorResult, TextCompilerResult};
use crate::entities::tag::Tag;
use crate::entities::tokens::Token;

pub struct GenSeqRequest<'a> {
    pub tags: &'a [Tag],
    pub max_request_len: usize,
    pub max_tagged_len: usize,
    pub try_end_after_len: Option<usize>,
}

pub trait IGenerator {
    fn gen_seq(&self, request: GenSeqRequest) -> GeneratorResult<Vec<Token>>;
}

pub trait ITextCompiler {
    fn compile(&self, tokens: Vec<Token>) -> TextCompilerResult<String>;
}
