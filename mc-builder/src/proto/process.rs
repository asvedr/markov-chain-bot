use std::collections::HashMap;
use std::ops::Range;

use easy_sqlite::errors::DbResult;
use mc_base::entities::chain::NextTokenTransition;
use mc_base::entities::tag::Tag;
use mc_base::entities::tokens::Token;
use mc_base::entities::words::WordType;

pub trait IWordMapMaker {
    fn make_word_map(&self, tokens: &[&String]) -> HashMap<String, WordType>;
}

pub trait IWordProcessor {
    fn update_words(&self, tokens: &[Token]) -> DbResult<()>;
}

pub trait IChainBuilder {
    fn build_begins_with_tags(
        &self,
        tokens: &[&[(Token, Vec<Tag>)]],
        chain_len: usize,
    ) -> Vec<(NextTokenTransition, Vec<Tag>)>;
    fn build_with_tags(
        &self,
        tokens: &[(Token, Vec<Tag>)],
        chain_len: usize,
    ) -> Vec<(NextTokenTransition, Vec<Tag>)>;
    fn build_without_tags(
        &self,
        tokens: &[(Token, Vec<Tag>)],
        chain_len: usize,
    ) -> Vec<NextTokenTransition>;
}

pub struct TokenProcessorRequest<'a> {
    pub tokens: &'a [(Token, Vec<Tag>)],
    pub only_from_begin: Range<usize>,
    pub with_tags: Range<usize>,
    pub without_tags: Range<usize>,
}

impl<'a> Default for TokenProcessorRequest<'a> {
    fn default() -> Self {
        const TOKENS: &[(Token, Vec<Tag>)] = &[];
        Self {
            tokens: TOKENS,
            only_from_begin: 0..0,
            with_tags: 0..0,
            without_tags: 0..0,
        }
    }
}

pub trait ITokenProcessor {
    fn process_tokens(&self, request: TokenProcessorRequest) -> DbResult<()>;
}

// ------------------------
// -------- IMPLS ---------
// ------------------------

impl<T: IWordMapMaker> IWordMapMaker for &'_ T {
    fn make_word_map(&self, tokens: &[&String]) -> HashMap<String, WordType> {
        (**self).make_word_map(tokens)
    }
}

impl<T: IWordProcessor> IWordProcessor for &'_ T {
    fn update_words(&self, tokens: &[Token]) -> DbResult<()> {
        (**self).update_words(tokens)
    }
}

impl<T: IChainBuilder> IChainBuilder for &'_ T {
    fn build_begins_with_tags(
        &self,
        tokens: &[&[(Token, Vec<Tag>)]],
        chain_len: usize,
    ) -> Vec<(NextTokenTransition, Vec<Tag>)> {
        (**self).build_begins_with_tags(tokens, chain_len)
    }
    fn build_with_tags(
        &self,
        tokens: &[(Token, Vec<Tag>)],
        chain_len: usize,
    ) -> Vec<(NextTokenTransition, Vec<Tag>)> {
        (**self).build_with_tags(tokens, chain_len)
    }
    fn build_without_tags(
        &self,
        tokens: &[(Token, Vec<Tag>)],
        chain_len: usize,
    ) -> Vec<NextTokenTransition> {
        (**self).build_without_tags(tokens, chain_len)
    }
}

impl<T: ITokenProcessor> ITokenProcessor for &'_ T {
    fn process_tokens(&self, request: TokenProcessorRequest) -> DbResult<()> {
        (**self).process_tokens(request)
    }
}
