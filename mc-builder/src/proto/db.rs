use std::collections::HashMap;
use std::str::FromStr;

use easy_sqlite::errors::DbResult;

use crate::entities::chain::AsDbNextToken;
use crate::entities::common::Lang;
use crate::entities::tag::Tag;
use crate::entities::tokens::Token;
use crate::entities::words::WordType;

pub trait IWordRepo {
    fn get_words(&self, keys: &[&String]) -> DbResult<HashMap<String, WordType>>;
    fn save_words(&self, words: &[(String, WordType)]) -> DbResult<()>;
}

pub trait ITagRepo {
    fn save_tags(&self, tag_to_transition: &[(Tag, u64)]) -> DbResult<()>;
}

pub trait INextTokenRepo {
    fn inc_transitions(&self, transitions: &[impl AsDbNextToken])
        -> DbResult<HashMap<String, u64>>;
    fn get_transitions_for_seq(&self, seq: &str) -> DbResult<Vec<(Token, usize)>>;
    fn get_transitions_for_seq_with_tags(
        &self,
        seq: &str,
        tags: &[Tag],
    ) -> DbResult<Vec<(Token, usize)>>;
}

const KEY_LANG: &str = "lang";

pub trait ISettingsRepo {
    fn get(&self, key: &str) -> DbResult<Option<String>>;
    fn set(&self, key: &str, val: &str) -> DbResult<()>;
    fn get_lang(&self) -> DbResult<Lang> {
        match self.get(KEY_LANG)? {
            Some(code) => match Lang::from_str(&code) {
                Ok(val) => Ok(val),
                Err(_) => panic!("unsupported lang: {}", code),
            },
            _ => Ok(Lang::DEFAULT),
        }
    }
    fn set_lang(&self, lang: Lang) -> DbResult<()> {
        self.set(KEY_LANG, &lang.to_string())
    }
}

// ------------------------
// -------- IMPLS ---------
// ------------------------

impl<T: IWordRepo> IWordRepo for &'_ T {
    fn get_words(&self, keys: &[&String]) -> DbResult<HashMap<String, WordType>> {
        (**self).get_words(keys)
    }

    fn save_words(&self, words: &[(String, WordType)]) -> DbResult<()> {
        (**self).save_words(words)
    }
}

impl<T: ITagRepo> ITagRepo for &'_ T {
    fn save_tags(&self, tag_to_transition: &[(Tag, u64)]) -> DbResult<()> {
        (**self).save_tags(tag_to_transition)
    }
}

impl<T: INextTokenRepo> INextTokenRepo for &'_ T {
    fn inc_transitions(
        &self,
        transitions: &[impl AsDbNextToken],
    ) -> DbResult<HashMap<String, u64>> {
        (**self).inc_transitions(transitions)
    }

    fn get_transitions_for_seq(&self, seq: &str) -> DbResult<Vec<(Token, usize)>> {
        (**self).get_transitions_for_seq(seq)
    }

    fn get_transitions_for_seq_with_tags(
        &self,
        seq: &str,
        tags: &[Tag],
    ) -> DbResult<Vec<(Token, usize)>> {
        (**self).get_transitions_for_seq_with_tags(seq, tags)
    }
}

impl<T: ISettingsRepo> ISettingsRepo for &'_ T {
    fn get(&self, key: &str) -> DbResult<Option<String>> {
        (**self).get(key)
    }
    fn set(&self, key: &str, val: &str) -> DbResult<()> {
        (**self).set(key, val)
    }
}
