pub fn chunkify<T>(src: &[T], chunk_size: usize) -> Vec<&[T]> {
    let mut result = Vec::new();
    let max = src.len() / chunk_size;
    for i in 0..max {
        let chunk = &src[chunk_size * i..chunk_size * (i + 1)];
        result.push(chunk);
    }
    if max * chunk_size < src.len() {
        result.push(&src[max * chunk_size..])
    }
    result
}

pub fn chunkify_vec<T>(src: Vec<T>, chunk_size: usize) -> Vec<Vec<T>> {
    let mut result = Vec::new();
    let mut chunk = Vec::new();
    for item in src {
        chunk.push(item);
        if chunk.len() == chunk_size {
            result.push(chunk);
            chunk = Vec::new()
        }
    }
    if !chunk.is_empty() {
        result.push(chunk);
    }
    result
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_chunkify_1() {
        assert_eq!(
            chunkify(&["a", "b", "c", "d"], 1),
            &[&["a"], &["b"], &["c"], &["d"]],
        );
        assert_eq!(
            chunkify_vec(vec!["a", "b", "c", "d"], 1),
            vec![vec!["a"], vec!["b"], vec!["c"], vec!["d"]],
        )
    }

    #[test]
    fn test_chunkify_2() {
        assert_eq!(
            chunkify(&["a", "b", "c", "d"], 2),
            &[&["a", "b"], &["c", "d"]],
        );
        assert_eq!(
            chunkify_vec(vec!["a", "b", "c", "d"], 2),
            vec![vec!["a", "b"], vec!["c", "d"]],
        );
    }

    #[test]
    fn test_chunkify_3() {
        assert_eq!(
            chunkify(&["a", "b", "c", "d"], 3),
            &[&["a", "b", "c"] as &[&str], &["d"]],
        );
    }

    #[test]
    fn test_chunkify_4() {
        assert_eq!(chunkify(&["a", "b", "c", "d"], 4), &[&["a", "b", "c", "d"]],);
    }

    #[test]
    fn test_chunkify_5() {
        assert_eq!(chunkify(&["a", "b", "c", "d"], 5), &[&["a", "b", "c", "d"]],);
    }
}
