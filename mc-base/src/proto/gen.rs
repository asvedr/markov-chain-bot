use crate::entities::errors::{GeneratorResult, TextCompilerResult};
use crate::entities::tag::Tag;
use crate::entities::tokens::Token;

pub type TokenFn<'a> = &'a dyn Fn(&[Token]) -> bool;

#[derive(Clone)]
pub struct GenSeqRequest<'a, 'b, 'c> {
    pub tags: &'a [Tag],
    pub seq_window_size: usize,
    pub use_tag_if: TokenFn<'b>,
    pub use_prob_in_random_if: TokenFn<'c>,
    pub try_end_after_len: Option<usize>,
}

pub trait IGenerator {
    fn gen_seq(&self, request: GenSeqRequest) -> GeneratorResult<Vec<Token>>;
}

pub trait ITextCompiler {
    fn compile(&self, tokens: Vec<Token>) -> TextCompilerResult<String>;
}

impl<'a, 'b, 'c> GenSeqRequest<'a, 'b, 'c> {
    pub fn new() -> GenSeqRequest<'static, 'static, 'static> {
        fn always_true(_: &[Token]) -> bool {
            true
        }
        GenSeqRequest {
            tags: &[],
            seq_window_size: 3,
            use_tag_if: &always_true,
            use_prob_in_random_if: &always_true,
            try_end_after_len: None,
        }
    }

    pub fn set_tags<'x>(self, tags: &'x [Tag]) -> GenSeqRequest<'x, 'b, 'c> {
        GenSeqRequest {
            tags,
            seq_window_size: self.seq_window_size,
            use_tag_if: self.use_tag_if,
            use_prob_in_random_if: self.use_prob_in_random_if,
            try_end_after_len: self.try_end_after_len,
        }
    }

    pub fn set_seq_window_size(mut self, seq_window_size: usize) -> Self {
        self.seq_window_size = seq_window_size;
        self
    }

    pub fn set_use_tag_if<'x>(self, use_tag_if: TokenFn<'x>) -> GenSeqRequest<'a, 'x, 'c> {
        GenSeqRequest {
            tags: self.tags,
            seq_window_size: self.seq_window_size,
            use_tag_if,
            use_prob_in_random_if: self.use_prob_in_random_if,
            try_end_after_len: self.try_end_after_len,
        }
    }

    pub fn set_use_prob_in_random_if<'x>(
        self,
        use_prob_in_random_if: TokenFn<'x>,
    ) -> GenSeqRequest<'a, 'b, 'x> {
        GenSeqRequest {
            tags: self.tags,
            seq_window_size: self.seq_window_size,
            use_tag_if: self.use_tag_if,
            use_prob_in_random_if,
            try_end_after_len: self.try_end_after_len,
        }
    }

    pub fn set_try_end_after_len(mut self, try_end_after_len: Option<usize>) -> Self {
        self.try_end_after_len = try_end_after_len;
        self
    }
}
