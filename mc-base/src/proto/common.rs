use std::borrow::Cow;

pub trait IStemmer {
    fn stem<'a>(&self, word: &'a str) -> Cow<'a, str>;
}

// -----------------------
// ---- IMPLS ------------
// -----------------------

impl<T: IStemmer> IStemmer for &'_ T {
    fn stem<'a>(&self, word: &'a str) -> Cow<'a, str> {
        (**self).stem(word)
    }
}
