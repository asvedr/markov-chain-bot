use easy_sqlite::RSQLConnection;
use minnixrand::predictable::FiniteRandom;

use crate::entities::chain::NextTokenTransition;
use crate::entities::tag::Tag;
use crate::entities::tokens::Token;
use crate::impls::generator::Generator;
use crate::impls::sqlite::next_token_repo::NextTokenRepo;
use crate::proto::db::INextTokenRepo;
use crate::proto::gen::{GenSeqRequest, IGenerator};

fn make_generator(
    transitions: &[(NextTokenTransition, Vec<Tag>)],
    rnd: &[usize],
) -> Box<dyn IGenerator> {
    let repo = NextTokenRepo::new(RSQLConnection::new(":memory:").unwrap()).unwrap();
    repo.inc_transitions(transitions).unwrap();
    let gen = Generator::new(FiniteRandom::new(rnd.to_vec()), repo);
    Box::new(gen)
}

#[test]
fn test_empty() {
    let gen = make_generator(&[], &[1]);
    let res = gen
        .gen_seq(
            GenSeqRequest::new()
                .set_seq_window_size(3)
                .set_use_tag_if(&|tokens| tokens.len() <= 3),
        )
        .unwrap();
    assert_eq!(
        res,
        &[
            Token::StartOfSentence,
            Token::EndOfSentence,
            Token::EndOfBlock
        ]
    )
}

fn tokens_seq() -> Vec<(NextTokenTransition, Vec<Tag>)> {
    vec![
        (
            NextTokenTransition {
                seq_tokens: "s".to_string(),
                next_token: Token::Word("hi".to_string()),
                count: 1,
            },
            vec![],
        ),
        (
            NextTokenTransition {
                seq_tokens: "s whi".to_string(),
                next_token: Token::Word("it".to_string()),
                count: 1,
            },
            vec![],
        ),
        (
            NextTokenTransition {
                seq_tokens: "s whi wit".to_string(),
                next_token: Token::Punctuation('\''),
                count: 1,
            },
            vec![],
        ),
        (
            NextTokenTransition {
                seq_tokens: "whi wit p'".to_string(),
                next_token: Token::Word("s".to_string()),
                count: 1,
            },
            vec![],
        ),
        (
            NextTokenTransition {
                seq_tokens: "wit p' ws".to_string(),
                next_token: Token::Word("me".to_string()),
                count: 1,
            },
            vec![],
        ),
        (
            NextTokenTransition {
                seq_tokens: "p' ws wme".to_string(),
                next_token: Token::EndOfSentence,
                count: 1,
            },
            vec![],
        ),
        (
            NextTokenTransition {
                seq_tokens: "ws wme e".to_string(),
                next_token: Token::EndOfBlock,
                count: 1,
            },
            vec![],
        ),
    ]
}

fn updated_tokens_seq() -> Vec<(NextTokenTransition, Vec<Tag>)> {
    let mut seq = tokens_seq();
    seq.append(&mut vec![
        (
            NextTokenTransition {
                seq_tokens: "s whi".to_string(),
                next_token: Token::Word("faggot".to_string()),
                count: 3,
            },
            vec![],
        ),
        (
            NextTokenTransition {
                seq_tokens: "wit p' ws".to_string(),
                next_token: Token::Word("doom".to_string()),
                count: 3,
            },
            vec![],
        ),
    ]);
    seq
}

#[test]
fn test_one_seq() {
    let gen = make_generator(&tokens_seq(), &[]);
    let seq = gen
        .gen_seq(
            GenSeqRequest::new()
                .set_seq_window_size(3)
                .set_use_tag_if(&|t| t.len() <= 3),
        )
        .unwrap();
    assert_eq!(
        seq,
        &[
            Token::StartOfSentence,
            Token::Word("hi".to_string()),
            Token::Word("it".to_string()),
            Token::Punctuation('\''),
            Token::Word("s".to_string()),
            Token::Word("me".to_string()),
            Token::EndOfSentence,
            Token::EndOfBlock,
        ]
    )
}

#[test]
fn test_choose_seq_1() {
    let gen = make_generator(&updated_tokens_seq(), &[0, 3]);
    let seq = gen
        .gen_seq(
            GenSeqRequest::new()
                .set_seq_window_size(3)
                .set_use_tag_if(&|t| t.len() <= 3),
        )
        .unwrap();
    assert_eq!(
        seq,
        &[
            Token::StartOfSentence,
            Token::Word("hi".to_string()),
            Token::Word("it".to_string()),
            Token::Punctuation('\''),
            Token::Word("s".to_string()),
            Token::Word("doom".to_string()),
            Token::EndOfSentence,
            Token::EndOfBlock,
        ]
    )
}

#[test]
fn test_choose_seq_2() {
    let gen = make_generator(&updated_tokens_seq(), &[2, 0]);
    let seq = gen
        .gen_seq(
            GenSeqRequest::new()
                .set_seq_window_size(3)
                .set_use_tag_if(&|t| t.len() < 3),
        )
        .unwrap();
    assert_eq!(
        seq,
        &[
            Token::StartOfSentence,
            Token::Word("hi".to_string()),
            Token::Word("faggot".to_string()),
            Token::EndOfSentence,
            Token::EndOfBlock,
        ]
    )
}

#[test]
fn test_fix_bug_with_sum() {
    let trs = &[
        (
            NextTokenTransition {
                seq_tokens: "s".to_string(),
                next_token: Token::Word("a".to_string()),
                count: 1,
            },
            vec![],
        ),
        (
            NextTokenTransition {
                seq_tokens: "s".to_string(),
                next_token: Token::Word("b".to_string()),
                count: 1,
            },
            vec![],
        ),
        (
            NextTokenTransition {
                seq_tokens: "s".to_string(),
                next_token: Token::Word("c".to_string()),
                count: 1,
            },
            vec![],
        ),
        (
            NextTokenTransition {
                seq_tokens: "s".to_string(),
                next_token: Token::Word("d".to_string()),
                count: 1,
            },
            vec![],
        ),
    ];
    let gen = make_generator(trs, &[2, 0]);
    let seq = gen
        .gen_seq(
            GenSeqRequest::new()
                .set_seq_window_size(3)
                .set_use_tag_if(&|t| t.len() <= 3),
        )
        .unwrap();
    let expected = &[
        Token::StartOfSentence,
        Token::Word("c".to_string()),
        Token::EndOfSentence,
        Token::EndOfBlock,
    ];
    assert_eq!(seq, expected)
}
