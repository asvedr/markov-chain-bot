use easy_sqlite::RSQLConnection;

use crate::entities::common::Lang;
use crate::entities::constants::{ILangConstants, UNI_QUOTE_CLOSE, UNI_QUOTE_OPEN};
use crate::entities::lang_specific;
use crate::entities::tokens::Token;
use crate::impls::sqlite::word_repo::WordRepo;
use crate::impls::text_compiler::TextCompiler;
use crate::proto::gen::ITextCompiler;

struct LangConsts;

const CONSTS: LangConsts = LangConsts;

impl ILangConstants for LangConsts {
    fn alphabet(&self) -> &'static str {
        lang_specific::get(Lang::En).alphabet()
    }

    fn lang(&self) -> Lang {
        Lang::En
    }

    fn acceptable_punctuation(&self) -> &'static str {
        lang_specific::get(Lang::En).alphabet()
    }

    fn punctuation_replacements_compiletime(&self) -> &'static [(char, char)] {
        &[(UNI_QUOTE_OPEN, '<'), (UNI_QUOTE_CLOSE, '>')]
    }
}

fn make_compiler() -> Box<dyn ITextCompiler> {
    let cnn = RSQLConnection::new(":memory:").unwrap();
    let compiler = TextCompiler {
        word_repo: WordRepo::new(cnn).unwrap(),
        chunk_size: 10,
        consts: &CONSTS,
    };
    Box::new(compiler)
}

#[test]
fn test_compile_text() {
    let tokens = vec![
        Token::StartOfSentence,
        Token::Word("hello".to_string()),
        Token::Punctuation(','),
        Token::Word("my".to_string()),
        Token::Punctuation(UNI_QUOTE_OPEN),
        Token::Word("friend".to_string()),
        Token::Punctuation(UNI_QUOTE_CLOSE),
        Token::Word("he".to_string()),
        Token::Word("he".to_string()),
        Token::EndOfSentence,
        Token::EndOfBlock,
    ];
    let expected = "Hello, my <friend> he he";
    let text = make_compiler().compile(tokens).unwrap();
    assert_eq!(text, expected);
}
