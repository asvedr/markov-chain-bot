use minnixrand::IRandomizer;

use crate::entities::errors::GeneratorResult;
use crate::entities::tag::{Tag, TagType};
use crate::entities::tokens::Token;
use crate::proto::db::INextTokenRepo;
use crate::proto::gen::{GenSeqRequest, IGenerator};

pub struct Generator<R: IRandomizer<usize>, NTR: INextTokenRepo> {
    pub randomizer: R,
    pub next_token_repo: NTR,
    ending_tags: Vec<Tag>,
}

impl<R: IRandomizer<usize>, NTR: INextTokenRepo> Generator<R, NTR> {
    pub fn new(randomizer: R, next_token_repo: NTR) -> Self {
        let tag = Tag {
            tag_type: TagType::LastSentenceInBlock,
            key: "".to_string(),
        };
        Self {
            randomizer,
            next_token_repo,
            ending_tags: vec![tag],
        }
    }
}

impl<R: IRandomizer<usize>, NTR: INextTokenRepo> Generator<R, NTR> {
    fn next_token(&self, tokens: &[Token]) -> GeneratorResult<Vec<(Token, usize)>> {
        let transitions = self
            .next_token_repo
            .get_transitions_for_seq(&Self::make_seq(tokens))?;
        Ok(transitions)
        // self.choose(transitions)
    }

    fn next_token_with_tags(
        &self,
        tokens: &[Token],
        tags: &[Tag],
    ) -> GeneratorResult<Vec<(Token, usize)>> {
        let transitions = self
            .next_token_repo
            .get_transitions_for_seq_with_tags(&Self::make_seq(tokens), tags)?;
        Ok(transitions)
        // self.choose(transitions)
    }

    fn next_token_try_end(&self, tokens: &[Token]) -> GeneratorResult<Vec<(Token, usize)>> {
        let seq = Self::make_seq(tokens);
        let mut variants = self
            .next_token_repo
            .get_transitions_for_seq_with_tags(&seq, &self.ending_tags)?;
        if variants.is_empty() {
            variants = self.next_token_repo.get_transitions_for_seq(&seq)?;
        }
        Ok(variants)
        // self.choose(variants)
    }

    fn next_token_try_end_with_tags(
        &self,
        tokens: &[Token],
        tags: &[Tag],
    ) -> GeneratorResult<Vec<(Token, usize)>> {
        let seq = Self::make_seq(tokens);
        let tags = tags
            .iter()
            .chain(self.ending_tags.iter())
            .cloned()
            .collect::<Vec<_>>();
        let mut variants = self
            .next_token_repo
            .get_transitions_for_seq_with_tags(&seq, &tags)?;
        if variants.is_empty() {
            variants = self
                .next_token_repo
                .get_transitions_for_seq_with_tags(&seq, &self.ending_tags)?;
        }
        if variants.is_empty() {
            variants = self.next_token_repo.get_transitions_for_seq(&seq)?;
        }
        Ok(variants)
        // self.choose(variants)
    }

    fn choose_prob(&self, mut transitions: Vec<(Token, usize)>) -> GeneratorResult<Token> {
        match transitions.len() {
            0 => {
                eprintln!("warning: transitions is empty");
                return Ok(Token::EndOfSentence);
            }
            1 => return Ok(transitions.swap_remove(0).0),
            _ => (),
        }
        let threshold = self.get_r_threshold(&transitions)?;
        let mut sum = 0;
        let mut last = Token::EndOfBlock;
        for (token, count) in transitions {
            if count + sum > threshold {
                return Ok(token);
            }
            sum += count;
            last = token;
        }
        eprintln!("warning: sum was not reached");
        Ok(last)
    }

    fn choose_full_rand(&self, mut transitions: Vec<(Token, usize)>) -> GeneratorResult<Token> {
        let index = match transitions.len() {
            0 => {
                eprintln!("warning: transitions is empty");
                return Ok(Token::EndOfSentence);
            }
            1 => 0,
            len => self.randomizer.gen()? % len,
        };
        let (token, _) = transitions.swap_remove(index);
        Ok(token)
    }

    #[inline]
    fn get_r_threshold(&self, transitions: &[(Token, usize)]) -> GeneratorResult<usize> {
        let sum = transitions.iter().map(|(_, cnt)| *cnt).sum::<usize>();
        let r_value = self.randomizer.gen()?;
        Ok(r_value % sum)
    }

    #[inline]
    fn make_seq(tokens: &[Token]) -> String {
        tokens
            .iter()
            .map(|tkn| format!("{}", tkn))
            .collect::<Vec<_>>()
            .join(" ")
    }

    #[inline]
    fn cut_to_request(tokens: &[Token], max_len: usize) -> &[Token] {
        if tokens.len() <= max_len {
            return tokens;
        }
        let len = tokens.len();
        &tokens[len - max_len..]
    }
}

impl<R: IRandomizer<usize>, NTR: INextTokenRepo> IGenerator for Generator<R, NTR> {
    fn gen_seq(&self, request: GenSeqRequest) -> GeneratorResult<Vec<Token>> {
        let mut tokens = vec![Token::StartOfSentence];
        let tags: &[Tag] = request.tags;
        while !matches!(tokens.last(), Some(Token::EndOfBlock)) {
            let request_cut = Self::cut_to_request(&tokens, request.seq_window_size);
            let tag_required = (request.use_tag_if)(&tokens);
            let ending_required = match request.try_end_after_len {
                Some(ref limit) => tokens.len() > *limit,
                _ => false,
            };
            let variants = match (ending_required, tag_required) {
                (true, true) => self.next_token_try_end_with_tags(request_cut, tags),
                (true, false) => self.next_token_try_end(request_cut),
                (_, true) => self.next_token_with_tags(request_cut, tags),
                (_, false) => self.next_token(request_cut),
            }?;
            let prob_required = (request.use_prob_in_random_if)(&tokens);
            let next = if prob_required {
                self.choose_prob(variants)
            } else {
                self.choose_full_rand(variants)
            }?;
            // let next = match request.try_end_after_len {
            //     Some(ref limit) if t_len > *limit && t_len <= request.max_tagged_len => {
            //         self.next_token_try_end_with_tags(request_cut, &request.tags)
            //     }
            //     Some(ref limit) if t_len > *limit => self.next_token_try_end(request_cut),
            //     _ if t_len > request.max_tagged_len => self.next_token(request_cut),
            //     _ => self.next_token_with_tags(request_cut, &request.tags),
            // }?;
            let last_token = tokens.last().unwrap();
            if next == Token::EndOfSentence && *last_token == Token::EndOfSentence {
                tokens.push(Token::EndOfBlock);
            } else {
                tokens.push(next);
            }
        }
        Ok(tokens)
    }
}
