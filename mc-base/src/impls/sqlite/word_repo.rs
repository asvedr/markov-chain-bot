use easy_sqlite::errors::DbResult;
use easy_sqlite::traits::repo::IConnection;
use easy_sqlite::{IDbRepo, IExecutor, INewDbRepo, ITable, TableManager};
use rusqlite::ToSql;
use std::collections::HashMap;

use crate::entities::words::WordType;
use crate::impls::sqlite::tables::WordTable;
use crate::proto::db::IWordRepo;

pub struct WordRepo<Cnn: IConnection + IExecutor> {
    cnn: Cnn,
}

impl<Cnn: IConnection + IExecutor> WordRepo<Cnn> {
    pub fn new(cnn: Cnn) -> DbResult<Self> {
        let tbl: TableManager<&Cnn, WordTable> = TableManager::create(&cnn);
        tbl.init()?;
        Ok(Self { cnn })
    }
}

impl<Cnn: IConnection + IExecutor> IWordRepo for WordRepo<Cnn> {
    fn get_words(&self, keys: &[&String]) -> DbResult<HashMap<String, WordType>> {
        if keys.is_empty() {
            return Ok(Default::default());
        }
        let mut query = format!(
            "SELECT data, `type` FROM {tbl} WHERE data IN (?",
            tbl = WordTable::NAME,
        );
        for _ in 1..keys.len() {
            query.push_str(",?")
        }
        query.push(')');
        let params = keys
            .iter()
            .map(|key| -> &dyn ToSql { key })
            .collect::<Vec<_>>();
        let rows: Vec<(String, isize)> = self
            .cnn
            .get_many(&query, &params, |row| Ok((row.get(0)?, row.get(1)?)))?;
        let map = rows
            .into_iter()
            .map(|(key, val)| -> (String, WordType) { (key, WordType::from_int(val).unwrap()) })
            .collect();
        Ok(map)
    }

    fn save_words(&self, words: &[(String, WordType)]) -> DbResult<()> {
        if words.is_empty() {
            return Ok(());
        }
        let mut query = format!(
            "INSERT OR REPLACE INTO {tbl} (data, `type`) VALUES (?, ?)",
            tbl = WordTable::NAME,
        );
        for _ in 1..words.len() {
            query.push_str(", (?, ?)");
        }
        let mut params = Vec::with_capacity(words.len() * 2);
        for (dt, tp) in words {
            params.push(dt as &dyn ToSql);
            params.push(tp.as_int() as &dyn ToSql);
        }
        self.cnn.execute(&query, &params)
    }
}
