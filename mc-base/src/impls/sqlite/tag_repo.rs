use easy_sqlite::errors::DbResult;
use easy_sqlite::traits::repo::IConnection;
use easy_sqlite::{IDbRepo, IExecutor, INewDbRepo, ITable, TableManager};
use rusqlite::ToSql;

use crate::entities::tag::Tag;
use crate::impls::sqlite::tables::TagTable;
use crate::proto::db::ITagRepo;

pub struct TagRepo<Cnn: IConnection + IExecutor> {
    cnn: Cnn,
}

impl<Cnn: IConnection + IExecutor> TagRepo<Cnn> {
    pub fn new(cnn: Cnn) -> DbResult<Self> {
        TableManager::<&Cnn, TagTable>::create(&cnn).init()?;
        Ok(Self { cnn })
    }
}

impl<Cnn: IConnection + IExecutor> ITagRepo for TagRepo<Cnn> {
    fn save_tags(&self, tag_to_transition: &[(Tag, u64)]) -> DbResult<()> {
        let vals = vec!["(?, ?)"; tag_to_transition.len()].join(",");
        let mut params: Vec<&dyn ToSql> = Vec::with_capacity(tag_to_transition.len() * 2);
        for (tag, token_id) in tag_to_transition {
            params.push(tag);
            params.push(token_id);
        }
        let query = format!(
            "INSERT OR IGNORE INTO {tbl} (key, next_token_id) VALUES {vals}",
            tbl = TagTable::NAME,
            vals = vals,
        );
        self.cnn.execute(&query, &params)
    }
}
