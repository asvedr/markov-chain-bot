use crate::entities::words::WordType;
use easy_sqlite::RSQLConnection;

use crate::impls::sqlite::word_repo::WordRepo;
use crate::proto::db::IWordRepo;

fn word_repo() -> WordRepo<RSQLConnection> {
    WordRepo::new(RSQLConnection::new(":memory:").unwrap()).unwrap()
}

#[test]
fn test_none() {
    let repo = word_repo();
    let res = repo.get_words(&[&"abc".to_string()]).unwrap();
    assert!(res.is_empty());
}

#[test]
fn test_get() {
    let repo = word_repo();
    repo.save_words(&[
        ("how".to_string(), WordType::Common),
        ("are".to_string(), WordType::FirstCaps),
        ("you".to_string(), WordType::FullCaps),
    ])
    .unwrap();
    let got = repo
        .get_words(&[&"how".to_string(), &"are".to_string(), &"long".to_string()])
        .unwrap();
    assert_eq!(
        got,
        [
            ("how".to_string(), WordType::Common),
            ("are".to_string(), WordType::FirstCaps)
        ]
        .into_iter()
        .collect()
    )
}

#[test]
fn test_update() {
    let repo = word_repo();
    repo.save_words(&[
        ("how".to_string(), WordType::Common),
        ("are".to_string(), WordType::FirstCaps),
        ("you".to_string(), WordType::FullCaps),
    ])
    .unwrap();
    repo.save_words(&[
        ("are".to_string(), WordType::Common),
        ("you".to_string(), WordType::FullCaps),
        ("mad".to_string(), WordType::FirstCaps),
    ])
    .unwrap();
    let got = repo
        .get_words(&[&"how".to_string(), &"are".to_string(), &"mad".to_string()])
        .unwrap();
    assert_eq!(
        got,
        [
            ("how".to_string(), WordType::Common),
            ("are".to_string(), WordType::Common),
            ("mad".to_string(), WordType::FirstCaps)
        ]
        .into_iter()
        .collect()
    )
}
