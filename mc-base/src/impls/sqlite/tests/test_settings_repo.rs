use crate::entities::common::Lang;
use crate::impls::sqlite::settings_repo::SettingsRepo;
use crate::proto::db::ISettingsRepo;
use easy_sqlite::RSQLConnection;

fn make_repo() -> SettingsRepo<RSQLConnection> {
    SettingsRepo::new(RSQLConnection::new(":memory:").unwrap()).unwrap()
}

#[test]
fn test_get_default_lang() {
    assert_eq!(make_repo().get_lang(), Ok(Lang::DEFAULT))
}

#[test]
fn test_set_lang() {
    let repo = make_repo();
    repo.set_lang(Lang::En).unwrap();
    assert_eq!(repo.get_lang(), Ok(Lang::En));
    repo.set_lang(Lang::Ru).unwrap();
    assert_eq!(repo.get_lang(), Ok(Lang::Ru));
}
