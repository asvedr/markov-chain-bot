use easy_sqlite::errors::DbResult;
use easy_sqlite::RSQLConnection;
use std::collections::HashMap;

use crate::entities::chain::{AsDbNextToken, NextTokenTransition};
use crate::entities::tag::{Tag, TagType};
use crate::entities::tokens::Token;
use crate::impls::sqlite::next_token_repo::NextTokenRepo;
use crate::impls::sqlite::tag_repo::TagRepo;
use crate::proto::db::{INextTokenRepo, ITagRepo};

struct MockRepo {
    ntt: NextTokenRepo<RSQLConnection>,
    tag: TagRepo<RSQLConnection>,
}

impl INextTokenRepo for MockRepo {
    fn inc_transitions(
        &self,
        transitions: &[impl AsDbNextToken],
    ) -> DbResult<HashMap<String, u64>> {
        self.ntt.inc_transitions(transitions)
    }

    fn get_transitions_for_seq(&self, seq: &str) -> DbResult<Vec<(Token, usize)>> {
        let mut res = self.ntt.get_transitions_for_seq(seq)?;
        res.sort();
        Ok(res)
    }

    fn get_transitions_for_seq_with_tags(
        &self,
        seq: &str,
        tags: &[Tag],
    ) -> DbResult<Vec<(Token, usize)>> {
        let mut res = self.ntt.get_transitions_for_seq_with_tags(seq, tags)?;
        res.sort();
        Ok(res)
    }
}

impl ITagRepo for MockRepo {
    fn save_tags(&self, tag_to_transition: &[(Tag, u64)]) -> DbResult<()> {
        self.tag.save_tags(tag_to_transition)
    }
}

fn make_repo() -> MockRepo {
    let cnn = RSQLConnection::new(":memory:").unwrap();
    let ntt = NextTokenRepo::new(cnn.clone()).unwrap();
    let tag = TagRepo::new(cnn).unwrap();
    MockRepo { ntt, tag }
}

#[test]
fn test_get_seq_with_tag() {
    let repo = make_repo();
    let map = repo
        .inc_transitions(&[
            NextTokenTransition {
                seq_tokens: "abc".to_string(),
                next_token: Token::StartOfSentence,
                count: 2,
            },
            NextTokenTransition {
                seq_tokens: "abc".to_string(),
                next_token: Token::Punctuation(','),
                count: 1,
            },
        ])
        .unwrap();
    let id1 = *map.get("abc>s").unwrap();
    let id2 = *map.get("abc>p,").unwrap();
    let tag1 = Tag {
        tag_type: TagType::Word,
        key: "a".to_string(),
    };
    let tag2 = Tag {
        tag_type: TagType::QBad,
        key: Default::default(),
    };
    let tag3 = Tag {
        tag_type: TagType::QNeed,
        key: Default::default(),
    };
    repo.save_tags(&[
        (tag1.clone(), id1),
        (tag2.clone(), id1),
        (tag2.clone(), id2),
        (tag3.clone(), id2),
    ])
    .unwrap();

    assert_eq!(
        repo.get_transitions_for_seq_with_tags("abc", &[tag1.clone()])
            .unwrap(),
        &[(Token::StartOfSentence, 2)],
    );

    assert_eq!(
        repo.get_transitions_for_seq_with_tags("abc", &[tag3.clone()])
            .unwrap(),
        &[(Token::Punctuation(','), 1)],
    );

    assert_eq!(
        repo.get_transitions_for_seq_with_tags("abc", &[tag2])
            .unwrap(),
        &[(Token::StartOfSentence, 2), (Token::Punctuation(','), 1)],
    );

    assert_eq!(
        repo.get_transitions_for_seq_with_tags("abc", &[tag1, tag3])
            .unwrap(),
        &[(Token::StartOfSentence, 2), (Token::Punctuation(','), 1)],
    );
}
