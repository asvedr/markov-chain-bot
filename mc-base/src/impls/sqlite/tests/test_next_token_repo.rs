use std::collections::HashMap;

use easy_sqlite::errors::DbResult;
use easy_sqlite::RSQLConnection;

use crate::entities::chain::{AsDbNextToken, NextTokenTransition};
use crate::entities::tag::Tag;
use crate::entities::tokens::Token;
use crate::impls::sqlite::next_token_repo::NextTokenRepo;
use crate::proto::db::INextTokenRepo;

struct MockRepo {
    inner: NextTokenRepo<RSQLConnection>,
}

impl INextTokenRepo for MockRepo {
    fn inc_transitions(
        &self,
        transitions: &[impl AsDbNextToken],
    ) -> DbResult<HashMap<String, u64>> {
        self.inner.inc_transitions(transitions)
    }

    fn get_transitions_for_seq(&self, seq: &str) -> DbResult<Vec<(Token, usize)>> {
        let mut res = self.inner.get_transitions_for_seq(seq)?;
        res.sort();
        Ok(res)
    }

    fn get_transitions_for_seq_with_tags(
        &self,
        seq: &str,
        tags: &[Tag],
    ) -> DbResult<Vec<(Token, usize)>> {
        let mut res = self.inner.get_transitions_for_seq_with_tags(seq, tags)?;
        res.sort();
        Ok(res)
    }
}

fn make_repo() -> MockRepo {
    let inner = NextTokenRepo::new(RSQLConnection::new(":memory:").unwrap()).unwrap();
    MockRepo { inner }
}

#[test]
fn test_empty() {
    let repo = make_repo();
    assert!(repo.get_transitions_for_seq(&"s").unwrap().is_empty());
}

#[test]
fn test_inc_value() {
    let repo = make_repo();
    repo.inc_transitions(&[
        NextTokenTransition {
            seq_tokens: "a b c".to_string(),
            next_token: Token::EndOfSentence,
            count: 2,
        },
        NextTokenTransition {
            seq_tokens: "a b c".to_string(),
            next_token: Token::StartOfSentence,
            count: 1,
        },
    ])
    .unwrap();
    let res = repo.get_transitions_for_seq("a b c").unwrap();
    assert_eq!(
        res,
        &[(Token::StartOfSentence, 1), (Token::EndOfSentence, 2)]
    );
    repo.inc_transitions(&[
        NextTokenTransition {
            seq_tokens: "a b c".to_string(),
            next_token: Token::EndOfSentence,
            count: 2,
        },
        NextTokenTransition {
            seq_tokens: "c b a".to_string(),
            next_token: Token::StartOfSentence,
            count: 10,
        },
    ])
    .unwrap();
    let res = repo.get_transitions_for_seq("a b c").unwrap();
    assert_eq!(
        res,
        &[(Token::StartOfSentence, 1), (Token::EndOfSentence, 4)]
    );
    let res = repo.get_transitions_for_seq("c b a").unwrap();
    assert_eq!(res, &[(Token::StartOfSentence, 10)]);
}
