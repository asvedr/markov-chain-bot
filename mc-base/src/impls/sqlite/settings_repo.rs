use crate::impls::sqlite::tables::SettingsTable;
use crate::proto::db::ISettingsRepo;
use easy_sqlite::errors::{DbError, DbResult};
use easy_sqlite::traits::repo::IConnection;
use easy_sqlite::{IDbRepo, IExecutor, INewDbRepo, TableManager};

pub struct SettingsRepo<Cnn: IConnection + IExecutor> {
    cnn: Cnn,
}

impl<Cnn: IConnection + IExecutor> SettingsRepo<Cnn> {
    pub fn new(cnn: Cnn) -> DbResult<Self> {
        let tbl: TableManager<&Cnn, SettingsTable> = TableManager::create(&cnn);
        tbl.init()?;
        Ok(Self { cnn })
    }
}

impl<Cnn: IConnection + IExecutor> ISettingsRepo for SettingsRepo<Cnn> {
    fn get(&self, key: &str) -> DbResult<Option<String>> {
        let query = "SELECT val FROM settings WHERE key = ?";
        match self.cnn.get_one(query, &[&key], |row| row.get(0)) {
            Ok(val) => Ok(Some(val)),
            Err(DbError::NotFound(_)) => Ok(None),
            Err(err) => Err(err),
        }
    }

    fn set(&self, key: &str, val: &str) -> DbResult<()> {
        let query = "INSERT OR REPLACE INTO settings (key, val) VALUES (?, ?)";
        self.cnn.execute(query, &[&key, &val])
    }
}
