use easy_sqlite::{ITable, SStr, SVec};

pub struct WordTable;
pub struct NextTokenTable;
pub struct TagTable;
pub struct SettingsTable;

impl ITable for WordTable {
    const PK: SStr = "data";
    const NAME: SStr = "word";
    const COLUMNS: SVec<(SStr, SStr)> =
        &[("data", "TEXT PRIMARY KEY"), ("type", "INTEGER NOT NULL")];
}

impl ITable for NextTokenTable {
    const PK: SStr = "id";
    const NAME: SStr = "next_token";
    const COLUMNS: SVec<(SStr, SStr)> = &[
        ("id", "INTEGER PRIMARY KEY"),
        ("token", "TEXT NOT NULL"),
        ("seq", "TEXT NOT NULL"),
        ("slug", "TEXT NOT NULL"),
        ("count", "INTEGER NOT NULL"),
    ];
    const INDEXES: SVec<SStr> = &["token", "seq"];
    const UNIQUE: SVec<SStr> = &["slug"];
}

impl ITable for TagTable {
    const PK: SStr = "id";
    const NAME: SStr = "tag";
    const COLUMNS: SVec<(SStr, SStr)> = &[
        ("id", "INTEGER PRIMARY KEY"),
        ("key", "TEXT NOT NULL"),
        ("next_token_id", "INTEGER NOT NULL"),
    ];
    const INDEXES: SVec<SStr> = &["key"];
    const UNIQUE: SVec<SStr> = &["key,next_token_id"];
    const FOREIGN_KEYS: SVec<(SStr, SStr, SStr)> =
        &[("next_token_id", NextTokenTable::NAME, NextTokenTable::PK)];
}

impl ITable for SettingsTable {
    const PK: SStr = "key";
    const NAME: SStr = "settings";
    const COLUMNS: SVec<(SStr, SStr)> = &[("key", "TEXT PRIMARY KEY"), ("val", "TEXT NOT NULL")];
}
