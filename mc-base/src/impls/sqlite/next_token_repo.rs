use std::collections::HashMap;

use std::str::FromStr;

use easy_sqlite::errors::DbResult;
use easy_sqlite::traits::repo::IConnection;
use easy_sqlite::{IDbRepo, IExecutor, INewDbRepo, ITable, TableManager};
use rusqlite::ToSql;

use crate::entities::chain::{AsDbNextToken, DbNextToken};
use crate::entities::tag::Tag;
use crate::entities::tokens::Token;
use crate::impls::sqlite::tables::{NextTokenTable, TagTable};
use crate::proto::db::INextTokenRepo;

pub struct NextTokenRepo<Cnn: IConnection + IExecutor> {
    cnn: Cnn,
}

impl<Cnn: IConnection + IExecutor> NextTokenRepo<Cnn> {
    pub fn new(cnn: Cnn) -> DbResult<Self> {
        TableManager::<&Cnn, NextTokenTable>::create(&cnn).init()?;
        Ok(Self { cnn })
    }

    fn create_new(&self, db_objs: &[&DbNextToken]) -> DbResult<()> {
        if db_objs.is_empty() {
            return Ok(());
        }
        let tmpl = vec!["(?, ?, ?, ?)"; db_objs.len()].join(",");
        let query = format!(
            "INSERT OR IGNORE INTO {tbl} (token, seq, slug, count) VALUES {vals}",
            tbl = NextTokenTable::NAME,
            vals = tmpl,
        );
        let mut params: Vec<&dyn ToSql> = Default::default();
        for db_obj in db_objs {
            params.push(&db_obj.next_token);
            params.push(&db_obj.seq);
            params.push(&db_obj.slug);
            params.push(&db_obj.count);
        }
        self.cnn.execute(&query, &params)
    }

    fn update_counts(&self, db_objs: HashMap<String, usize>) -> DbResult<()> {
        if db_objs.is_empty() {
            return Ok(());
        }
        let mut params: Vec<&dyn ToSql> = Default::default();
        let len = db_objs.len();
        let query = format!(
            r#"UPDATE  {tbl}
                SET `count` = CASE `slug`
                    {whenthen_t}
                END
            WHERE `slug` IN ({slugs_t})
            "#,
            tbl = NextTokenTable::NAME,
            whenthen_t = vec!["WHEN ? THEN ?"; len].join("\n"),
            slugs_t = vec!["?"; len].join(",")
        );
        for (slug, cnt) in db_objs.iter() {
            params.push(slug);
            params.push(cnt);
        }
        for slug in db_objs.keys() {
            params.push(slug);
        }
        self.cnn.execute(&query, &params)
    }

    fn get_counts(&self, db_objs: &[DbNextToken]) -> DbResult<HashMap<String, usize>> {
        let params = db_objs
            .iter()
            .map(|db_obj| -> &dyn ToSql { &db_obj.slug })
            .collect::<Vec<_>>();
        let query = format!(
            "SELECT slug, `count` FROM {tbl} WHERE slug IN ({slugs})",
            tbl = NextTokenTable::NAME,
            slugs = vec!["?"; params.len()].join(",")
        );
        let map = self
            .cnn
            .get_many::<(String, usize), _>(&query, &params, |row| Ok((row.get(0)?, row.get(1)?)))?
            .into_iter()
            .collect::<HashMap<_, _>>();
        Ok(map)
    }

    fn get_id_map(&self, db_objs: Vec<DbNextToken>) -> DbResult<HashMap<String, u64>> {
        let params = db_objs
            .iter()
            .map(|db_obj| -> &dyn ToSql { &db_obj.slug })
            .collect::<Vec<_>>();
        let tmpl = vec!["?"; db_objs.len()].join(",");
        let query = format!(
            "SELECT slug, id FROM {tbl} WHERE slug IN ({slugs})",
            tbl = NextTokenTable::NAME,
            slugs = tmpl
        );
        let map = self
            .cnn
            .get_many::<(String, u64), _>(&query, &params, |row| Ok((row.get(0)?, row.get(1)?)))?
            .into_iter()
            .collect();
        Ok(map)
    }

    fn parse_token(src: String) -> rusqlite::Result<Token> {
        Token::from_str(&src).map_err(|_| rusqlite::Error::InvalidQuery)
    }
}

impl<Cnn: IConnection + IExecutor> INextTokenRepo for NextTokenRepo<Cnn> {
    fn inc_transitions(
        &self,
        transitions: &[impl AsDbNextToken],
    ) -> DbResult<HashMap<String, u64>> {
        let db_objs = transitions
            .iter()
            .map(|tr| tr.as_db_token())
            .collect::<Vec<_>>();
        let mut slug_count_map: HashMap<String, usize> = self.get_counts(&db_objs)?;
        let mut new_rows = Vec::new();
        for db_obj in db_objs.iter() {
            if let Some(cnt) = slug_count_map.get_mut(&db_obj.slug) {
                *cnt += db_obj.count;
            } else {
                new_rows.push(db_obj);
            }
        }
        self.update_counts(slug_count_map)?;
        self.create_new(&new_rows)?;
        let res = self.get_id_map(db_objs)?;
        Ok(res)
    }

    fn get_transitions_for_seq(&self, seq: &str) -> DbResult<Vec<(Token, usize)>> {
        let query = format!(
            "SELECT token, `count` FROM {tbl} WHERE seq = ? ORDER BY `count`, `id`",
            tbl = NextTokenTable::NAME,
        );
        self.cnn.get_many(&query, &[&seq], |row| {
            Ok((Self::parse_token(row.get(0)?)?, row.get(1)?))
        })
    }

    fn get_transitions_for_seq_with_tags(
        &self,
        seq: &str,
        tags: &[Tag],
    ) -> DbResult<Vec<(Token, usize)>> {
        if tags.is_empty() {
            return self.get_transitions_for_seq(seq);
        }
        let query = format!(
            r#"
            SELECT ntt.token, ntt.`count`
                FROM {ntt_tbl} as ntt JOIN {tag_tbl} as tag
                    ON ntt.id = tag.next_token_id
            WHERE ntt.seq = ? AND tag.key IN ({keys})
            ORDER BY ntt.`count`, ntt.`id`
            "#,
            ntt_tbl = NextTokenTable::NAME,
            tag_tbl = TagTable::NAME,
            keys = vec!["?"; tags.len()].join(","),
        );
        let mut params: Vec<&dyn ToSql> = vec![&seq];
        for tag in tags {
            params.push(tag)
        }
        self.cnn.get_many(&query, &params, |row| {
            Ok((Self::parse_token(row.get(0)?)?, row.get(1)?))
        })
    }
}
