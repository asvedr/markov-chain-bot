use std::borrow::Cow;

use rust_stemmers::Stemmer as ExtStemmer;

use crate::entities::common::Lang;
use crate::entities::lang_specific;
use crate::proto::common::IStemmer;

pub struct Stemmer {
    inner: ExtStemmer,
    whitelist: &'static [(&'static str, &'static str)],
}

impl Stemmer {
    pub fn new(lang: Lang) -> Self {
        let whitelist = lang_specific::get(lang).specific_words();
        Self {
            inner: ExtStemmer::create(lang.into()),
            whitelist,
        }
    }

    fn get_wl_value(&self, word: &str) -> Option<&'static str> {
        for (key, val) in self.whitelist {
            if *key == word {
                return Some(val);
            }
        }
        None
    }
}

impl IStemmer for Stemmer {
    fn stem<'a>(&self, word: &'a str) -> Cow<'a, str> {
        if let Some(key) = self.get_wl_value(word) {
            return Cow::Borrowed(key);
        }
        self.inner.stem(word)
    }
}
