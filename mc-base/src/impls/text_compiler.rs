use std::collections::HashMap;

use crate::entities::constants::{ILangConstants, PUNCTUATION_OPEN_CLOSE};
use crate::entities::errors::{TextCompilerError, TextCompilerResult};
use crate::entities::tokens::Token;
use crate::entities::words::WordType;
use crate::proto::db::IWordRepo;
use crate::proto::gen::ITextCompiler;
use crate::utils::seq::chunkify_vec;

pub struct TextCompiler<WR: IWordRepo> {
    pub word_repo: WR,
    pub chunk_size: usize,
    pub consts: &'static dyn ILangConstants,
}

#[derive(PartialEq)]
enum TextState {
    Common,
    SentenceBegin,
    AfterOpenSymbol,
}

struct State {
    text_state: TextState,
    result: String,
    open_symbols: Vec<char>,
}

impl<WR: IWordRepo> TextCompiler<WR> {
    fn process_chunk(&self, state: &mut State, chunk: Vec<Token>) -> TextCompilerResult<()> {
        let words = chunk
            .iter()
            .filter_map(|tkn| match tkn {
                Token::Word(val) => Some(val),
                _ => None,
            })
            .collect::<Vec<_>>();
        let map = self.word_repo.get_words(&words)?;
        for token in chunk {
            match token {
                Token::StartOfSentence => state.text_state = TextState::SentenceBegin,
                Token::EndOfSentence | Token::EndOfBlock => (),
                Token::Punctuation(val) => self.add_punctuation(state, val)?,
                Token::Word(val) => Self::add_word(state, &map, val)?,
            }
        }
        Ok(())
    }

    fn add_punctuation(&self, state: &mut State, val: char) -> TextCompilerResult<()> {
        if let Some(sym) = Self::get_open_to_close(val) {
            if state.open_symbols.last() != Some(&sym) {
                return Err(TextCompilerError::OpenClosePunctuationFailed);
            }
            state.open_symbols.pop();
            state.result.push(self.replace_punct_symbol(val));
            return Ok(());
        } else if Self::is_open_symbol(val) {
            state.open_symbols.push(val);
            state.result.push(' ');
            state.result.push(self.replace_punct_symbol(val));
            state.text_state = TextState::AfterOpenSymbol;
            return Ok(());
        }
        let sym = self.replace_punct_symbol(val);
        state.result.push(sym);
        if self.consts.no_space_after().contains(sym) {
            state.text_state = TextState::AfterOpenSymbol;
        } else {
            state.text_state = TextState::Common;
        }
        Ok(())
    }

    fn replace_punct_symbol(&self, sym: char) -> char {
        for (from, to) in self.consts.punctuation_replacements_compiletime() {
            if *from == sym {
                return *to;
            }
        }
        sym
    }

    fn is_open_symbol(sym: char) -> bool {
        for (key, _) in PUNCTUATION_OPEN_CLOSE {
            if *key == sym {
                return true;
            }
        }
        false
    }

    fn get_open_to_close(sym: char) -> Option<char> {
        for (op, cl) in PUNCTUATION_OPEN_CLOSE {
            if *cl == sym {
                return Some(*op);
            }
        }
        None
    }

    fn add_word(
        state: &mut State,
        map: &HashMap<String, WordType>,
        key: String,
    ) -> TextCompilerResult<()> {
        let word_type = match map.get(&key) {
            Some(val) => *val,
            None => {
                eprintln!("warning: word {:?} not found", key);
                WordType::Common
            }
        };
        let word = match word_type {
            WordType::FullCaps => key.to_uppercase(),
            WordType::FirstCaps => Self::first_upper(key),
            WordType::Common if Self::is_begin_of_sentence(state) => Self::first_upper(key),
            WordType::Common => key,
        };
        if !(state.text_state == TextState::AfterOpenSymbol || state.result.is_empty()) {
            state.result.push(' ');
        }
        state.result.push_str(&word);
        state.text_state = TextState::Common;
        Ok(())
    }

    #[inline]
    fn is_begin_of_sentence(state: &State) -> bool {
        matches!(state.text_state, TextState::SentenceBegin)
    }

    #[inline]
    fn first_upper(word: String) -> String {
        let mut chars = word.chars();
        chars.next().unwrap().to_uppercase().chain(chars).collect()
    }
}

impl<WR: IWordRepo> ITextCompiler for TextCompiler<WR> {
    fn compile(&self, tokens: Vec<Token>) -> TextCompilerResult<String> {
        let mut state = State {
            text_state: TextState::SentenceBegin,
            result: "".to_string(),
            open_symbols: Vec::new(),
        };
        for chunk in chunkify_vec(tokens, self.chunk_size) {
            self.process_chunk(&mut state, chunk)?;
        }
        Ok(state.result)
    }
}
