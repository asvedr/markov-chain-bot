use rust_stemmers::Algorithm;
use std::str::FromStr;

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub enum Lang {
    Ru,
    En,
}

impl Lang {
    pub const DEFAULT: Self = Self::Ru;
}

impl From<Lang> for Algorithm {
    fn from(value: Lang) -> Self {
        match value {
            Lang::Ru => Algorithm::Russian,
            Lang::En => Algorithm::English,
        }
    }
}

impl FromStr for Lang {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "ru" => Ok(Self::Ru),
            "en" => Ok(Self::En),
            _ => Err(()),
        }
    }
}

impl ToString for Lang {
    fn to_string(&self) -> String {
        match self {
            Self::Ru => "ru",
            Self::En => "en",
        }
        .to_string()
    }
}
