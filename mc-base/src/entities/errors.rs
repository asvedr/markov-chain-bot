use easy_sqlite::errors::DbError;
use minnixrand::error::RandomizerError;

pub type GeneratorResult<T> = Result<T, GeneratorError>;
pub type TextCompilerResult<T> = Result<T, TextCompilerError>;

#[derive(Debug)]
pub enum GeneratorError {
    DB(DbError),
    Randomizer(String),
    InputTokensAreEmpty,
}

#[derive(Debug)]
pub enum TextCompilerError {
    DB(DbError),
    WordNotFound(String),
    OpenClosePunctuationFailed,
}

impl From<DbError> for GeneratorError {
    fn from(value: DbError) -> Self {
        Self::DB(value)
    }
}

impl From<DbError> for TextCompilerError {
    fn from(value: DbError) -> Self {
        Self::DB(value)
    }
}

impl From<RandomizerError> for GeneratorError {
    fn from(value: RandomizerError) -> Self {
        Self::Randomizer(format!("{:?}", value))
    }
}
