use std::ops::Range;
use std::str::FromStr;

pub struct Ranges {
    pub tagged: Range<usize>,
    pub untagged: Range<usize>,
}

impl Ranges {
    #[inline(always)]
    fn duplicate(link: &Range<usize>) -> (&Range<usize>, &Range<usize>) {
        (link, link)
    }

    #[inline(always)]
    fn get_both(&self) -> (&Range<usize>, &Range<usize>) {
        if self.tagged == (0..0) {
            return Self::duplicate(&self.untagged);
        }
        if self.untagged == (0..0) {
            return Self::duplicate(&self.tagged);
        }
        (&self.tagged, &self.untagged)
    }

    pub fn min_start(&self) -> usize {
        let (a, b) = self.get_both();
        a.start.min(b.start)
    }

    pub fn max_end(&self) -> usize {
        let (a, b) = self.get_both();
        a.end.max(b.end)
    }
}

impl FromStr for Ranges {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        fn parse_rng(s: &str) -> Result<Range<usize>, ()> {
            let split = s.split("..").collect::<Vec<_>>();
            if split.len() != 2 {
                return Err(());
            }
            let from = usize::from_str(split[0]).map_err(|_| ())?;
            let to = usize::from_str(split[1]).map_err(|_| ())?;
            Ok(from..to)
        }

        let split = s.split('|').collect::<Vec<_>>();
        if split.len() != 2 {
            return Err(());
        }
        Ok(Self {
            tagged: parse_rng(split[0])?,
            untagged: parse_rng(split[1])?,
        })
    }
}

impl ToString for Ranges {
    fn to_string(&self) -> String {
        format!(
            "{}..{}|{}..{}",
            self.tagged.start, self.tagged.end, self.untagged.start, self.untagged.end,
        )
    }
}
