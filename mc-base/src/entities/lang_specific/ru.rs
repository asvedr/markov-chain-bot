use crate::entities::common::Lang;
use crate::entities::constants::ILangConstants;

pub const THIS_K: &str = "это";
pub const THIS_V: &str = ":это";

pub struct Constants;

pub const SINGLETON: Constants = Constants;

impl ILangConstants for Constants {
    fn alphabet(&self) -> &'static str {
        "абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
    }

    fn lang(&self) -> Lang {
        Lang::Ru
    }

    fn acceptable_punctuation(&self) -> &'static str {
        "\"“”«»"
    }

    fn specific_words(&self) -> &'static [(&'static str, &'static str)] {
        &[(THIS_K, THIS_V)]
    }

    fn no_space_after(&self) -> &'static str {
        "-"
    }

    fn word_replacements_parsetime(&self) -> &'static [(&'static str, &'static str)] {
        &[
            ("т.д.", "так далее"),
            ("т. д.", "так далее"),
            ("т.п.", "тому подобное"),
            ("т. п.", "тому подобное"),
            ("т.к.", "так как"),
            ("т. к.", "так как"),
            ("т. е.", "то есть"),
            ("т.е.", "то есть"),
        ]
    }
}
