use crate::entities::common::Lang;
use crate::entities::constants::ILangConstants;

pub mod en;
pub mod ru;

const AVAILABLE: &[&'static dyn ILangConstants] = &[&en::SINGLETON, &ru::SINGLETON];

pub fn get(lang: Lang) -> &'static dyn ILangConstants {
    for consts in AVAILABLE {
        if lang == consts.lang() {
            return *consts;
        }
    }
    panic!("Can not find constants for lang: {:?}", lang)
}
