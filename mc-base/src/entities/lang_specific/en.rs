use crate::entities::common::Lang;
use crate::entities::constants::{ILangConstants, UNI_QUOTE_CLOSE, UNI_QUOTE_OPEN};

pub struct Constants;

pub const SINGLETON: Constants = Constants;

impl ILangConstants for Constants {
    fn alphabet(&self) -> &'static str {
        "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
    }

    fn lang(&self) -> Lang {
        Lang::En
    }

    fn acceptable_punctuation(&self) -> &'static str {
        "\"'`"
    }

    fn punctuation_replacements_compiletime(&self) -> &'static [(char, char)] {
        &[(UNI_QUOTE_OPEN, '"'), (UNI_QUOTE_CLOSE, '"')]
    }
}
