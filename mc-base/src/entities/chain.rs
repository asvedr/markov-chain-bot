use crate::entities::tokens::Token;

#[derive(Debug, Eq, PartialEq)]
pub struct NextTokenTransition {
    pub seq_tokens: String,
    pub next_token: Token,
    pub count: usize,
}

impl NextTokenTransition {
    pub fn key(&self) -> String {
        format!("{}>{}", self.seq_tokens, self.next_token)
    }
}

pub struct DbNextToken {
    pub seq: String,
    pub next_token: String,
    pub slug: String,
    pub count: usize,
}

pub trait AsDbNextToken {
    fn as_db_token(&self) -> DbNextToken;
}

impl AsDbNextToken for NextTokenTransition {
    fn as_db_token(&self) -> DbNextToken {
        DbNextToken {
            seq: self.seq_tokens.clone(),
            next_token: format!("{}", self.next_token),
            slug: self.key(),
            count: self.count,
        }
    }
}

impl<T> AsDbNextToken for (NextTokenTransition, T) {
    fn as_db_token(&self) -> DbNextToken {
        self.0.as_db_token()
    }
}
