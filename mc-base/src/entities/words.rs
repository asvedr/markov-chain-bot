#[derive(Debug, Eq, PartialEq, Copy, Clone)]
pub enum WordType {
    Common,
    FirstCaps,
    FullCaps,
}

impl WordType {
    const INT_MAP: &'static [(Self, isize)] =
        &[(Self::Common, 1), (Self::FirstCaps, 2), (Self::FullCaps, 3)];

    pub fn as_int(&self) -> &'static isize {
        for (k, v) in Self::INT_MAP {
            if k == self {
                return v;
            }
        }
        unreachable!()
    }

    pub fn from_int(val: isize) -> Option<Self> {
        for (v, k) in Self::INT_MAP {
            if *k == val {
                return Some(*v);
            }
        }
        None
    }
}
