use std::fmt;
use std::str::FromStr;

#[derive(Debug, Eq, PartialEq, Clone, Ord, PartialOrd)]
pub enum Token {
    StartOfSentence,
    EndOfSentence,
    EndOfBlock,
    Punctuation(char),
    Word(String),
}

impl fmt::Display for Token {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Token::StartOfSentence => write!(f, "s"),
            Token::EndOfSentence => write!(f, "e"),
            Token::EndOfBlock => write!(f, "E"),
            Token::Punctuation(sym) => write!(f, "p{}", sym),
            Token::Word(val) => write!(f, "w{}", val),
        }
    }
}

impl FromStr for Token {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut chars = s.chars();
        let code = match chars.next() {
            Some(val) => val,
            _ => return Err(()),
        };
        match code {
            's' => Ok(Self::StartOfSentence),
            'e' => Ok(Self::EndOfSentence),
            'E' => Ok(Self::EndOfBlock),
            'p' => match chars.next() {
                Some(val) => Ok(Self::Punctuation(val)),
                _ => Err(()),
            },
            'w' => Ok(Self::Word(chars.collect())),
            _ => Err(()),
        }
    }
}
