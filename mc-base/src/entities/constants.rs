use crate::entities::common::Lang;

pub const SENTENCE_TERMINATORS: &str = ".…?!";
pub const SHARED_PUNCTUATION: &str = ".…?!,;<>-&()[]/\\";
pub const SPACE: char = ' ';
pub const SYMBOLS_TO_REPLACE_ON_PREPARE: &[(&str, &str)] =
    &[("...", "…"), ("\t", " "), ("\r", " "), ("—", "-")];
pub const PUNCTUATION_OPEN_CLOSE: &[(char, char)] = &[('(', ')'), ('[', ']'), ('«', '»')];
pub const UNI_QUOTE_OPEN: char = '«';
pub const UNI_QUOTE_CLOSE: char = '»';
pub const SPECIFIC_WORD_PREFIX: &str = ":";

pub trait ILangConstants {
    fn alphabet(&self) -> &'static str;
    fn lang(&self) -> Lang;
    fn acceptable_punctuation(&self) -> &'static str {
        ""
    }
    fn specific_words(&self) -> &'static [(&'static str, &'static str)] {
        &[]
    }
    fn punctuation_replacements_compiletime(&self) -> &'static [(char, char)] {
        &[]
    }
    fn no_space_after(&self) -> &'static str {
        ""
    }
    fn word_replacements_parsetime(&self) -> &'static [(&'static str, &'static str)] {
        &[]
    }
}
