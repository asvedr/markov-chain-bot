use crate::entities::meta::Ranges;

#[test]
fn test_ranges() {
    let ranges = Ranges {
        tagged: (1..3),
        untagged: (2..4),
    };
    assert_eq!(ranges.min_start(), 1);
    assert_eq!(ranges.max_end(), 4);

    let ranges = Ranges {
        tagged: (3..7),
        untagged: (2..4),
    };
    assert_eq!(ranges.min_start(), 2);
    assert_eq!(ranges.max_end(), 7);
}

#[test]
fn test_ranges_to_one() {
    let ranges = Ranges {
        tagged: (0..0),
        untagged: (2..4),
    };
    assert_eq!(ranges.min_start(), 2);
    assert_eq!(ranges.max_end(), 4);

    let ranges = Ranges {
        tagged: (3..7),
        untagged: (0..0),
    };
    assert_eq!(ranges.min_start(), 3);
    assert_eq!(ranges.max_end(), 7);
}
