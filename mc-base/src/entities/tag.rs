use std::fmt;

use rusqlite::types::{ToSqlOutput, Value};
use rusqlite::ToSql;
use std::str::FromStr;

#[derive(Debug, Clone, Copy, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub enum TagType {
    Word,
    QWhatIs,
    QHowTo,
    QBad,
    QNeed,
    LastSentenceInBlock,
}

#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct Tag {
    pub tag_type: TagType,
    pub key: String,
}

impl fmt::Display for Tag {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}{}", self.tag_type.to_char(), self.key)
    }
}

impl ToSql for Tag {
    fn to_sql(&self) -> rusqlite::Result<ToSqlOutput<'_>> {
        Ok(ToSqlOutput::Owned(Value::Text(format!("{}", self))))
    }
}

impl TagType {
    const CONST_MAP: &'static [(Self, char, isize)] = &[
        (Self::Word, 'n', 1),
        (Self::QWhatIs, 'w', 2),
        (Self::QHowTo, 'h', 3),
        (Self::QBad, 'b', 4),
        (Self::QNeed, 'N', 5),
        (Self::LastSentenceInBlock, 'L', 6),
    ];

    pub fn all() -> Vec<Self> {
        Self::CONST_MAP.iter().map(|(val, _, _)| *val).collect()
    }

    pub fn to_isize(self) -> isize {
        for (k, _, v) in Self::CONST_MAP {
            if *k == self {
                return *v;
            }
        }
        unreachable!()
    }

    pub fn from_isize(val: isize) -> Self {
        for (v, _, k) in Self::CONST_MAP {
            if *k == val {
                return *v;
            }
        }
        unreachable!()
    }

    pub fn to_char(self) -> char {
        for (k, v, _) in Self::CONST_MAP {
            if *k == self {
                return *v;
            }
        }
        unreachable!()
    }

    pub fn from_char(val: char) -> Option<Self> {
        for (v, k, _) in Self::CONST_MAP {
            if *k == val {
                return Some(*v);
            }
        }
        None
    }
}

impl FromStr for Tag {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut chars = s.chars();
        let key = match chars.next() {
            None => return Err(()),
            Some(val) => val,
        };
        let tag_type = match TagType::from_char(key) {
            None => return Err(()),
            Some(val) => val,
        };
        Ok(Self {
            tag_type,
            key: chars.collect(),
        })
    }
}
