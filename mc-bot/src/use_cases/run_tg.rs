use std::thread;
use std::time::Duration;

use mddd::traits::IUseCase;

use crate::entities::config::Config;
use crate::entities::errors::RunnerError;
use crate::proto::bot::IBot;
use crate::runner;

pub struct RunTgUC {
    pub config: &'static Config,
    pub bot: &'static dyn IBot,
}

impl IUseCase for RunTgUC {
    type Request = ();
    type Error = RunnerError;

    fn short_description() -> String {
        "run tg bot".to_string()
    }

    fn execute(&mut self, _: ()) -> Result<(), Self::Error> {
        loop {
            let res = tokio::runtime::Builder::new_current_thread()
                .enable_all()
                .build()
                .unwrap()
                .block_on(runner::run(self.config, self.bot));
            if let Err(err) = res {
                eprintln!("TOPLEVEL ERROR: {:?}", err);
                thread::sleep(Duration::from_secs_f64(self.config.sleep_on_errors));
            }
        }
    }
}
