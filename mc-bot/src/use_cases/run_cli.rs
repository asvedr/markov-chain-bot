use std::io;
use std::io::Write;

use mddd::traits::IUseCase;

use crate::entities::errors::RunnerError;
use crate::proto::bot::IBot;

pub struct RunCliUC {
    pub bot: &'static dyn IBot,
}

impl IUseCase for RunCliUC {
    type Request = ();
    type Error = RunnerError;

    fn short_description() -> String {
        "run bot in cli mode".to_string()
    }

    fn execute(&mut self, _: ()) -> Result<(), Self::Error> {
        let stdin = io::stdin();
        let mut stdout = io::stdout();
        let mut line = String::new();
        loop {
            stdout.write_all(">> ".as_bytes())?;
            stdout.flush()?;
            line.clear();
            stdin.read_line(&mut line)?;
            match self.bot.execute("cli", line.trim()) {
                Ok((cmd, msg)) => println!("{}> {}", cmd, msg),
                Err(err) => println!("ERROR> {:?}", err),
            }
        }
    }
}
