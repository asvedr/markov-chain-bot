use easy_sqlite::RSQLConnection;
use mc_base::impls::sqlite::next_token_repo::NextTokenRepo;
use mc_base::impls::sqlite::settings_repo::SettingsRepo;
use mc_base::impls::sqlite::word_repo::WordRepo;
use mddd::macros::singleton_simple;

use crate::app::common::config;
use crate::impls::sqlite::settings_repo::ChatSettingsRepo;

singleton_simple!(
    fn content_cnn() -> RSQLConnection {
        match RSQLConnection::new(&config().content_db) {
            Ok(val) => val,
            Err(err) => panic!("Can not open content db: {:?}", err),
        }
    }
);

singleton_simple!(
    fn settings_cnn() -> RSQLConnection {
        match RSQLConnection::new(&config().settings_db) {
            Ok(val) => val,
            Err(err) => panic!("Can not open settings db: {:?}", err),
        }
    }
);

singleton_simple!(
    pub fn next_token_repo() -> NextTokenRepo<&'static RSQLConnection> {
        match NextTokenRepo::new(content_cnn()) {
            Ok(val) => val,
            Err(err) => panic!("Can not init next_token_repo: {:?}", err),
        }
    }
);

singleton_simple!(
    pub fn word_repo() -> WordRepo<&'static RSQLConnection> {
        match WordRepo::new(content_cnn()) {
            Ok(val) => val,
            Err(err) => panic!("Can not init word_repo: {:?}", err),
        }
    }
);

singleton_simple!(
    pub fn content_settings_repo() -> SettingsRepo<&'static RSQLConnection> {
        match SettingsRepo::new(content_cnn()) {
            Ok(val) => val,
            Err(err) => panic!("Can not init content_settings_repo: {:?}", err),
        }
    }
);

singleton_simple!(
    pub fn chat_settings_repo(
    ) -> ChatSettingsRepo<&'static RSQLConnection, &'static SettingsRepo<&'static RSQLConnection>>
    {
        match ChatSettingsRepo::new(settings_cnn(), content_settings_repo(), config()) {
            Ok(val) => val,
            Err(err) => panic!("Can not init chat_settings_repo: {:?}", err),
        }
    }
);
