use crate::app::common::config;
use crate::app::repos::content_settings_repo;
use crate::impls::bot::Bot;
use crate::impls::commands;
use crate::impls::tokenizer::Tokenizer;
use crate::proto::bot::ICommand;
use mddd::macros::singleton_simple;

type TCommand = Box<dyn ICommand>;

singleton_simple!(
    pub fn bot() -> Bot<TCommand, &'static [TCommand], Tokenizer> {
        use mc_base::proto::db::ISettingsRepo;

        let cs_repo = content_settings_repo();
        let lang = match cs_repo.get_lang() {
            Ok(val) => val,
            Err(err) => panic!("Can not get lang from content db: {:?}", err),
        };
        Bot::new(config(), commands::get(lang), Tokenizer::new(lang))
    }
);
