use crate::app::bot::bot;
use crate::app::common::config;

use crate::use_cases::run_cli::RunCliUC;
use crate::use_cases::run_tg::RunTgUC;

pub fn cli() -> RunCliUC {
    RunCliUC { bot: bot() }
}

pub fn tg() -> RunTgUC {
    RunTgUC {
        config: config(),
        bot: bot(),
    }
}
