use mddd::macros::singleton_simple;
use mddd::traits::IConfig;

use crate::entities::config::Config;

singleton_simple!(
    pub fn config() -> Config {
        match Config::build() {
            Ok(val) => val,
            Err(err) => panic!("Can not build config: {}", err),
        }
    }
);
