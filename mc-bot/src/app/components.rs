use easy_sqlite::RSQLConnection;
use mc_base::entities::lang_specific;
use mc_base::impls::generator::Generator;
use mc_base::impls::sqlite::next_token_repo::NextTokenRepo;
use mc_base::impls::sqlite::word_repo::WordRepo;
use mc_base::impls::stemmer::Stemmer;
use mc_base::impls::text_compiler::TextCompiler;

use mddd::macros::singleton_simple;
use minnixrand::urandom::Randomizer;

use crate::app::common::config;
use crate::app::repos::{content_settings_repo, next_token_repo, word_repo};
use crate::impls::text_generator::TextGenerator;

type TGenerator = Generator<Randomizer, &'static NextTokenRepo<&'static RSQLConnection>>;
type TTextCompiler = TextCompiler<&'static WordRepo<&'static RSQLConnection>>;

fn generator() -> TGenerator {
    let randomizer = match Randomizer::fast() {
        Ok(val) => val,
        Err(err) => panic!("Can not init randomizer: {:?}", err),
    };
    Generator::new(randomizer, next_token_repo())
}

fn text_compiler() -> TTextCompiler {
    use mc_base::proto::db::ISettingsRepo;

    let lang = match content_settings_repo().get_lang() {
        Ok(val) => val,
        Err(err) => panic!("Getting lang error: {:?}", err),
    };
    let consts = lang_specific::get(lang);
    TextCompiler {
        word_repo: word_repo(),
        chunk_size: config().text_compiler_chunk_size,
        consts,
    }
}

fn stemmer() -> Stemmer {
    use mc_base::proto::db::ISettingsRepo;

    let lang = match content_settings_repo().get_lang() {
        Ok(val) => val,
        Err(err) => panic!("Getting lang error: {:?}", err),
    };
    Stemmer::new(lang)
}

singleton_simple!(
    pub fn text_generator() -> TextGenerator<TGenerator, TTextCompiler, Stemmer> {
        TextGenerator::new(generator(), text_compiler(), stemmer(), config())
    }
);
