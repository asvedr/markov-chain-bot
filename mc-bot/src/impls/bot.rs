use std::marker::PhantomData;

use crate::entities::config::Config;
use crate::entities::errors::BotError;
use crate::proto::bot::{IBot, ICommand};
use crate::proto::common::ITokenizer;

pub struct Bot<C: ICommand, CL: AsRef<[C]>, T: ITokenizer> {
    commands: CL,
    trigger: String,
    default_cmd: Option<usize>,
    empty_response_cmd: Option<usize>,
    tokenizer: T,

    phantom: PhantomData<C>,
}

impl<C: ICommand, CL: AsRef<[C]>, T: ITokenizer> Bot<C, CL, T> {
    pub fn new(config: &Config, commands: CL, tokenizer: T) -> Self {
        let default_cmd = config
            .default_cmd
            .as_ref()
            .map(|name| Self::validate_cmd(name, commands.as_ref()));
        let empty_response_cmd = config
            .empty_response_cmd
            .as_ref()
            .map(|name| Self::validate_cmd(name, commands.as_ref()));
        Self {
            commands,
            trigger: config.trigger.clone(),
            tokenizer,
            default_cmd,
            empty_response_cmd,
            phantom: PhantomData::default(),
        }
    }

    fn gen_with_command(
        &self,
        command: &C,
        chat: &str,
        text: &str,
        tokens: &[&str],
    ) -> Result<(&'static str, String), BotError> {
        match command.execute(chat, text, tokens) {
            Ok(resp) => return Ok((command.name(), resp)),
            Err(BotError::EmptyResponse) => (),
            Err(err) => return Err(err),
        };
        let index = match self.empty_response_cmd {
            Some(ref val) => *val,
            _ => return Err(BotError::EmptyResponse),
        };
        let cmd = &self.commands.as_ref()[index];
        let resp = cmd.execute(chat, text, tokens)?;
        Ok((cmd.name(), resp))
    }

    fn validate_cmd(name: &str, commands: &[C]) -> usize {
        for i in 0..commands.len() {
            if commands[i].name() == name {
                return i;
            }
        }
        let available = commands
            .iter()
            .map(|cmd| cmd.name())
            .collect::<Vec<_>>()
            .join(",");
        panic!(
            "Command is invalid: {}. Available commands: {}",
            name, available
        )
    }
}

impl<C: ICommand, CL: AsRef<[C]>, T: ITokenizer> IBot for Bot<C, CL, T> {
    fn execute(&self, chat: &str, text: &str) -> Result<(&'static str, String), BotError> {
        if !text.to_lowercase().starts_with(&self.trigger) {
            return Err(BotError::NotMe);
        }
        let tokens_own = self.tokenizer.tokenize(text);
        let tokens_ref = tokens_own.iter().map(|t| -> &str { t }).collect::<Vec<_>>();
        let commands = self.commands.as_ref();
        for command in commands {
            if command.check_match(text, &tokens_ref) {
                return self.gen_with_command(command, chat, text, &tokens_ref);
            }
        }
        let index = match self.default_cmd {
            Some(ref val) => *val,
            _ => return Err(BotError::CommandNotFound),
        };
        self.gen_with_command(&commands[index], chat, text, &tokens_ref)
    }
}
