use std::collections::HashSet;

use mc_base::entities::common::Lang;
use mc_base::entities::lang_specific;
use mc_base::impls::stemmer::Stemmer;
use mc_base::proto::common::IStemmer;

use crate::proto::common::ITokenizer;

pub struct Tokenizer {
    alphabet: HashSet<char>,
    stemmer: Stemmer,
}

impl Tokenizer {
    pub fn new(lang: Lang) -> Self {
        let stemmer = Stemmer::new(lang);
        let alphabet = lang_specific::get(lang).alphabet().chars().collect();
        Self { alphabet, stemmer }
    }

    fn get_stem(&self, word: &mut String) -> String {
        let res = self.stemmer.stem(word).to_string();
        word.clear();
        res
    }
}

impl ITokenizer for Tokenizer {
    fn tokenize(&self, text: &str) -> Vec<String> {
        let mut result = Vec::new();
        let mut word = String::new();
        for sym in text.chars() {
            if self.alphabet.contains(&sym) {
                word.push(sym);
            } else if !word.is_empty() {
                result.push(self.get_stem(&mut word));
            }
        }
        if !word.is_empty() {
            result.push(self.get_stem(&mut word));
        }
        result
    }
}
