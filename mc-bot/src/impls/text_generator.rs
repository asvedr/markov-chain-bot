use std::borrow::Cow;

use mc_base::entities::tokens::Token;
use mc_base::proto::common::IStemmer;
use mc_base::proto::gen::{GenSeqRequest, IGenerator, ITextCompiler};

use crate::entities::config::Config;
use crate::entities::errors::BotError;
use crate::proto::common::ITextGenerator;

pub struct TextGenerator<G: IGenerator, C: ITextCompiler, S: IStemmer> {
    gen: G,
    compiler: C,
    stemmer: S,

    generate_retries: usize,
    max_symbols: usize,
}

impl<G: IGenerator, C: ITextCompiler, S: IStemmer> TextGenerator<G, C, S> {
    pub fn new(gen: G, compiler: C, stemmer: S, config: &Config) -> Self {
        Self {
            gen,
            compiler,
            stemmer,
            generate_retries: config.generate_phrase_max_tries,
            max_symbols: config.message_max_symbols,
        }
    }

    fn get_stems<'a>(&self, tokens: &'a [Token]) -> Vec<Cow<'a, str>> {
        tokens
            .iter()
            .filter_map(|tkn| match tkn {
                Token::Word(word) => Some(self.stemmer.stem(word)),
                _ => None,
            })
            .collect()
    }

    fn try_gen_text(
        &self,
        request: GenSeqRequest,
        check_valid: &impl Fn(&[&str]) -> bool,
    ) -> Result<String, BotError> {
        let tokens = self.gen.gen_seq(request)?;
        let cow_stems = self.get_stems(&tokens);
        let ptr_stems = cow_stems.iter().map(|cow| cow.as_ref()).collect::<Vec<_>>();
        if !check_valid(&ptr_stems) {
            return Err(BotError::EmptyResponse);
        }
        let text = self.compiler.compile(tokens)?;
        if text.is_empty() {
            return Err(BotError::EmptyResponse);
        } else {
            Ok(text)
        }
    }
}

impl<G: IGenerator, C: ITextCompiler, S: IStemmer> ITextGenerator for TextGenerator<G, C, S> {
    fn generate_text(
        &self,
        request: GenSeqRequest,
        check_valid: &impl Fn(&[&str]) -> bool,
    ) -> Result<String, BotError> {
        let mut last_succ_try = String::new();
        let mut err = BotError::EmptyResponse;
        for _ in 0..self.generate_retries {
            match self.try_gen_text(request.clone(), check_valid) {
                Ok(txt) if txt.len() <= self.max_symbols => return Ok(txt),
                Ok(txt) if last_succ_try.is_empty() => last_succ_try = txt,
                Ok(txt) if last_succ_try.len() > txt.len() => last_succ_try = txt,
                Ok(_) => (),
                Err(BotError::GenerationFailed(msg)) => err = BotError::GenerationFailed(msg),
                Err(BotError::EmptyResponse) => err = BotError::EmptyResponse,
                Err(err) => return Err(err),
            }
        }
        if last_succ_try.is_empty() {
            Err(err)
        } else {
            let cut = last_succ_try.chars().take(self.max_symbols).collect();
            Ok(cut)
        }
    }
}
