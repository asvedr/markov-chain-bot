use std::str::FromStr;

use easy_sqlite::errors::{DbError, DbResult};
use easy_sqlite::traits::repo::IConnection;
use easy_sqlite::IExecutor;
use mc_base::impls::sqlite::settings_repo::SettingsRepo;
use mc_base::proto::db::ISettingsRepo;

use crate::entities::config::Config;
use crate::proto::bot::IChatSettingsRepo;

pub struct ChatSettingsRepo<Cnn: IConnection + IExecutor, SR: ISettingsRepo> {
    inner: SettingsRepo<Cnn>,
    content_repo: SR,
    default_depth: usize,
}

impl<Cnn: IConnection + IExecutor, SR: ISettingsRepo> ChatSettingsRepo<Cnn, SR> {
    pub fn new(cnn: Cnn, content_repo: SR, config: &Config) -> DbResult<Self> {
        let inner = SettingsRepo::new(cnn)?;
        Ok(Self {
            inner,
            content_repo,
            default_depth: config.generation_depth,
        })
    }

    fn get_default_depth(&self) -> DbResult<usize> {
        let ranges = self.content_repo.get_ranges()?;
        let start = ranges.min_start();
        let end = ranges.max_end() - 1;
        Ok(start.max(self.default_depth).min(end))
    }

    fn depth_key(chat: &str) -> String {
        format!("depth|{}", chat)
    }
}

impl<Cnn: IConnection + IExecutor, SR: ISettingsRepo> IChatSettingsRepo
    for ChatSettingsRepo<Cnn, SR>
{
    fn get_depth(&self, chat: &str) -> DbResult<usize> {
        match self.inner.get(&Self::depth_key(chat))? {
            None => self.get_default_depth(),
            Some(val) => usize::from_str(&val)
                .map_err(|_| DbError::DeserializationError("not int".to_string())),
        }
    }

    fn set_depth(&self, chat: &str, val: usize) -> DbResult<()> {
        self.inner.set(&Self::depth_key(chat), &val.to_string())
    }
}
