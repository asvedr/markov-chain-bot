use mc_base::entities::common::Lang;

use crate::proto::bot::ICommand;

mod ru;

pub fn get(lang: Lang) -> &'static [Box<dyn ICommand>] {
    match lang {
        Lang::Ru => &**ru::all(),
        Lang::En => &[],
    }
}
