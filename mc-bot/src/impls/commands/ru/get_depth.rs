use crate::entities::errors::BotError;
use crate::proto::bot::{IChatSettingsRepo, ICommand};

pub struct Command<S: IChatSettingsRepo> {
    pub repo: S,
}

const GET: &[&str] = &["покаж", "скаж"];
const DEPTH: &str = "глубин";

impl<S: IChatSettingsRepo> ICommand for Command<S> {
    fn name(&self) -> &'static str {
        "get_depth"
    }

    fn check_match(&self, _: &str, tokens: &[&str]) -> bool {
        let len = tokens.len();
        if len < 2 {
            return false;
        }
        GET.contains(&tokens[len - 2]) && tokens[len - 1] == DEPTH
    }

    fn execute(&self, chat: &str, _: &str, _: &[&str]) -> Result<String, BotError> {
        let val = self.repo.get_depth(chat)?;
        Ok(val.to_string())
    }
}
