use crate::entities::errors::BotError;
use crate::proto::bot::{IChatSettingsRepo, ICommand};
use crate::proto::common::ITextGenerator;
use mc_base::proto::gen::GenSeqRequest;

pub struct Command<G: ITextGenerator, S: IChatSettingsRepo> {
    pub gen: G,
    pub repo: S,
}

impl<G: ITextGenerator, S: IChatSettingsRepo> ICommand for Command<G, S> {
    fn name(&self) -> &'static str {
        "notag"
    }

    fn check_match(&self, _: &str, _: &[&str]) -> bool {
        true
    }

    fn execute(&self, chat: &str, _: &str, _: &[&str]) -> Result<String, BotError> {
        let depth = self.repo.get_depth(chat)?;
        self.gen.generate_text(
            GenSeqRequest::new().set_seq_window_size(depth),
            // .set_try_end_after_len(Some(10)),
            &|_| true,
        )
    }
}
