use mc_base::entities::meta::Ranges;
use mc_base::proto::db::ISettingsRepo;
use std::ops::Range;
use std::str::FromStr;

use crate::entities::errors::BotError;
use crate::proto::bot::{IChatSettingsRepo, ICommand};

pub struct Command<S: ISettingsRepo, CS: IChatSettingsRepo> {
    pub settings: S,
    pub chat_settings: CS,
}

const SET: &[&str] = &["установ", "зада", "постав", "сохран"];
const DEPTH: &str = "глубин";

fn calc_minmax(
    rng: &Ranges,
    get_item: impl Fn(&Range<usize>) -> usize,
    cmp: impl Fn(usize, usize) -> usize,
) -> usize {
    if rng.tagged == (0..0) {
        return get_item(&rng.untagged);
    }
    if rng.untagged == (0..0) {
        return get_item(&rng.tagged);
    }
    cmp(get_item(&rng.tagged), get_item(&rng.untagged))
}

impl<S: ISettingsRepo, CS: IChatSettingsRepo> ICommand for Command<S, CS> {
    fn name(&self) -> &'static str {
        "set_depth"
    }

    fn check_match(&self, _: &str, tokens: &[&str]) -> bool {
        let len = tokens.len();
        if len < 2 {
            return false;
        }
        SET.contains(&tokens[len - 2]) && tokens[len - 1] == DEPTH
    }

    fn execute(&self, chat: &str, text: &str, _: &[&str]) -> Result<String, BotError> {
        let token = match text.split(' ').last() {
            None => return Err(BotError::command_error("depth not set")),
            Some(val) => val,
        };
        let val =
            usize::from_str(token).map_err(|_| BotError::command_error("depth not a number"))?;
        let ranges = self.settings.get_ranges()?;
        let min = calc_minmax(&ranges, |rng| rng.start, |a, b| a.min(b));
        let max = calc_minmax(&ranges, |rng| rng.end, |a, b| a.max(b));
        if !(min..max).contains(&val) {
            let msg = format!("Значение должно быть в промежутке {} <= x < {}", min, max,);
            return Ok(msg);
        }
        self.chat_settings.set_depth(chat, val)?;
        Ok("Готово".to_string())
    }
}
