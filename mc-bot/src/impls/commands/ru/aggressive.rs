use crate::entities::errors::BotError;
use crate::proto::bot::{IChatSettingsRepo, ICommand};
use crate::proto::common::ITextGenerator;
use mc_base::entities::tag::{Tag, TagType};
use mc_base::proto::gen::GenSeqRequest;

pub struct Command<G: ITextGenerator, S: IChatSettingsRepo> {
    pub gen: G,
    pub repo: S,
}

enum Templ {
    Node(&'static str, &'static [Templ]),
    Leaf(&'static str),
}

const WAY_NODE: &[Templ] = &[
    Templ::Node("на", &[Templ::Leaf("ху")]),
    Templ::Node("в", &[Templ::Leaf("жоп"), Templ::Leaf("пизд")]),
];

const SEQS: &[Templ] = &[
    Templ::Node("пошел", WAY_NODE),
    Templ::Node("ид", WAY_NODE),
    Templ::Node(
        "ты",
        &[Templ::Leaf("пидор"), Templ::Leaf("лох"), Templ::Leaf("ху")],
    ),
];

fn match_templ(templs: &[Templ], tokens: &[&str]) -> bool {
    for i in 0..tokens.len() {
        let token = tokens[i];
        for templ in templs {
            match templ {
                Templ::Node(key, rest) if token == *key => {
                    if match_templ(rest, &tokens[i + 1..]) {
                        return true;
                    }
                }
                Templ::Leaf(key) if token == *key => return true,
                _ => (),
            }
        }
    }
    false
}

impl<G: ITextGenerator, S: IChatSettingsRepo> ICommand for Command<G, S> {
    fn name(&self) -> &'static str {
        "aggressive"
    }

    fn check_match(&self, _: &str, tokens: &[&str]) -> bool {
        match_templ(SEQS, tokens)
    }

    fn execute(&self, chat: &str, _: &str, _: &[&str]) -> Result<String, BotError> {
        let depth = self.repo.get_depth(chat)?;
        let tag = Tag {
            tag_type: TagType::QBad,
            key: "".to_string(),
        };
        self.gen.generate_text(
            GenSeqRequest::new()
                .set_tags(&[tag])
                .set_seq_window_size(depth)
                .set_use_tag_if(&|_| true)
                // .set_try_end_after_len(Some(20))
                .set_use_prob_in_random_if(&|_| false),
            &|_| true,
        )
    }
}
