use crate::entities::errors::BotError;
use crate::proto::bot::{IChatSettingsRepo, ICommand};
use crate::proto::common::ITextGenerator;
use mc_base::entities::tag::{Tag, TagType};
use mc_base::proto::gen::GenSeqRequest;

pub struct Command<G: ITextGenerator, S: IChatSettingsRepo> {
    pub gen: G,
    pub repo: S,
}

const KEY1: &str = "что";
const KEY2: &str = "дума";
const KEY3: &[&str] = &["о", "об", "про"];

fn step1<'a>(tokens: &[&'a str]) -> Option<&'a str> {
    for i in 0..tokens.len() {
        if tokens[i] == KEY1 {
            return step2(&tokens[i + 1..]);
        }
    }
    None
}

fn step2<'a>(tokens: &[&'a str]) -> Option<&'a str> {
    for i in 0..tokens.len() {
        if tokens[i] == KEY2 {
            return step3(&tokens[i + 1..]);
        }
    }
    None
}

fn step3<'a>(tokens: &[&'a str]) -> Option<&'a str> {
    if tokens.is_empty() {
        return None;
    }
    for i in 0..tokens.len() - 1 {
        for key in KEY3 {
            if tokens[i] == *key {
                return Some(tokens[i + 1]);
            }
        }
    }
    None
}

impl<G: ITextGenerator, S: IChatSettingsRepo> Command<G, S> {
    fn get_token<'a>(&self, tokens: &[&'a str]) -> Option<&'a str> {
        step1(tokens)
    }
}

impl<G: ITextGenerator, S: IChatSettingsRepo> ICommand for Command<G, S> {
    fn name(&self) -> &'static str {
        "word"
    }

    fn check_match(&self, _: &str, tokens: &[&str]) -> bool {
        self.get_token(tokens).is_some()
    }

    fn execute(&self, chat: &str, _: &str, tokens: &[&str]) -> Result<String, BotError> {
        let token = self.get_token(tokens).unwrap();
        let tag = Tag {
            tag_type: TagType::Word,
            key: token.to_string(),
        };
        let depth = self.repo.get_depth(chat)?;
        self.gen.generate_text(
            GenSeqRequest::new()
                .set_tags(&[tag])
                .set_seq_window_size(depth)
                .set_use_tag_if(&|seq| seq.len() < depth),
            &|stems| stems.contains(&token),
        )
    }
}
