use crate::entities::errors::BotError;
use crate::proto::bot::{IChatSettingsRepo, ICommand};
use crate::proto::common::ITextGenerator;
use mc_base::entities::tag::{Tag, TagType};
use mc_base::proto::gen::GenSeqRequest;

pub struct Command<G: ITextGenerator, S: IChatSettingsRepo> {
    pub gen: G,
    pub repo: S,
}

const FIRST_WORD: &str = "что";
const NEED1: &str = "нужн";
const NEED2: &str = "над";
const SECOND_WORDS: &[&str] = &[NEED1, NEED2, "дела"];

fn check_resp(stems: &[&str]) -> bool {
    stems.contains(&NEED1) || stems.contains(&NEED2)
}

fn has_token(tokens: &[&str]) -> bool {
    for i in 0..tokens.len() {
        if tokens[i] == FIRST_WORD {
            return has_token_step_2(&tokens[i + 1..]);
        }
    }
    false
}

fn has_token_step_2(tokens: &[&str]) -> bool {
    for i in 0..tokens.len() {
        if SECOND_WORDS.contains(&tokens[i]) {
            return true;
        }
    }
    false
}

impl<G: ITextGenerator, S: IChatSettingsRepo> ICommand for Command<G, S> {
    fn name(&self) -> &'static str {
        "need"
    }

    fn check_match(&self, _: &str, tokens: &[&str]) -> bool {
        has_token(tokens)
    }

    fn execute(&self, chat: &str, _: &str, _tokens: &[&str]) -> Result<String, BotError> {
        let tag = Tag {
            tag_type: TagType::QNeed,
            key: "".to_string(),
        };
        let depth = self.repo.get_depth(chat)?;
        self.gen.generate_text(
            GenSeqRequest::new()
                .set_tags(&[tag])
                .set_seq_window_size(depth)
                .set_use_tag_if(&|seq| seq.len() < depth),
            // .set_try_end_after_len(Some(10)),
            &check_resp,
        )
    }
}
