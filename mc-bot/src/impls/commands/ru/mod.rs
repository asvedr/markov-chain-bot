mod aggressive;
mod get_depth;
mod need;
mod notag;
mod set_depth;
mod what_is;
mod word;

use mddd::macros::singleton_simple;

use crate::app::components::text_generator;
use crate::app::repos::{chat_settings_repo, content_settings_repo};
use crate::proto::bot::ICommand;

singleton_simple!(
    pub fn all() -> Vec<Box<dyn ICommand>> {
        let settings = content_settings_repo();
        let repo = chat_settings_repo();
        let gen = text_generator();
        vec![
            Box::new(get_depth::Command { repo }),
            Box::new(set_depth::Command {
                chat_settings: repo,
                settings,
            }),
            Box::new(what_is::Command { gen, repo }),
            Box::new(word::Command { gen, repo }),
            Box::new(need::Command { gen, repo }),
            Box::new(aggressive::Command { gen, repo }),
            Box::new(notag::Command { gen, repo }),
        ]
    }
);
