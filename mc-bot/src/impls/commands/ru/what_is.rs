use mc_base::entities::tag::{Tag, TagType};
use mc_base::proto::gen::GenSeqRequest;

use crate::entities::errors::BotError;
use crate::proto::bot::{IChatSettingsRepo, ICommand};
use crate::proto::common::ITextGenerator;

pub struct Command<G: ITextGenerator, S: IChatSettingsRepo> {
    pub gen: G,
    pub repo: S,
}

const SEQS: &[&[&str]] = &[&["что", "такое"], &["что", "есть"]];

fn get_token<'a>(tokens: &[&'a str]) -> Option<&'a str> {
    for i in 0..tokens.len() {
        for seq in SEQS {
            if !tokens[i..].starts_with(seq) {
                continue;
            }
            if i + 2 < tokens.len() {
                return Some(tokens[i + 2]);
            }
        }
    }
    None
}

impl<G: ITextGenerator, S: IChatSettingsRepo> ICommand for Command<G, S> {
    fn name(&self) -> &'static str {
        "what_is"
    }

    fn check_match(&self, _: &str, tokens: &[&str]) -> bool {
        get_token(tokens).is_some()
    }

    fn execute(&self, chat: &str, _: &str, tokens: &[&str]) -> Result<String, BotError> {
        let token = get_token(tokens).unwrap();
        let tag = Tag {
            tag_type: TagType::QWhatIs,
            key: token.to_string(),
        };
        let depth = self.repo.get_depth(chat)?;
        self.gen.generate_text(
            GenSeqRequest::new()
                .set_tags(&[tag])
                .set_seq_window_size(depth)
                .set_use_tag_if(&|seq| seq.len() < depth),
            &|stems| stems.contains(&token),
        )
    }
}
