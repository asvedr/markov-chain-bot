use futures::StreamExt;
use telegram_bot_ars::{Api, CanReplySendMessage, MessageKind, UpdateKind};

use crate::entities::config::Config;
use crate::entities::errors::{BotError, RunnerError};
use crate::proto::bot::IBot;

pub async fn run(config: &Config, executor: &dyn IBot) -> Result<(), RunnerError> {
    let api = Api::new(&config.token);

    // Fetch new updates via long poll method
    let mut stream = api.stream();
    while let Some(update) = stream.next().await {
        // If the received update contains a new message...
        let update = update?;
        let message = match update.kind {
            Some(UpdateKind::Message(val)) => val,
            _ => continue,
        };
        let text = match message.kind {
            MessageKind::Text { ref data, .. } => data,
            _ => continue,
        };
        println!("> {}", text);
        match executor.execute(&message.chat.id().to_string(), text) {
            Ok((_, resp)) => {
                api.send(message.text_reply(resp)).await?;
            }
            Err(BotError::CommandNotFound) | Err(BotError::NotMe) => continue,
            Err(BotError::EmptyResponse) => eprintln!("No response generated!"),
            Err(err) => eprintln!("\nERROR:\n  msg:{}\nerr: {:?}", text, err),
        }
    }
    Ok(())
}
