use easy_sqlite::errors::DbError;
use mc_base::entities::errors::{GeneratorError, TextCompilerError};
use std::io;

use telegram_bot_ars::Error as TgError;

#[derive(Debug)]
pub enum RunnerError {
    Bot(BotError),
    Tg(TgError),
    IO(io::Error),
}

#[derive(Debug)]
pub enum BotError {
    CommandNotFound,
    CommandError(String),
    NotMe,
    Db(DbError),
    GenerationFailed(String),
    EmptyResponse,
}

impl BotError {
    pub fn command_error<S: ToString>(msg: S) -> Self {
        Self::CommandError(msg.to_string())
    }
}

impl From<TgError> for RunnerError {
    fn from(value: TgError) -> Self {
        Self::Tg(value)
    }
}

impl From<io::Error> for RunnerError {
    fn from(value: io::Error) -> Self {
        Self::IO(value)
    }
}

impl From<DbError> for BotError {
    fn from(value: DbError) -> Self {
        Self::Db(value)
    }
}

impl From<GeneratorError> for BotError {
    fn from(value: GeneratorError) -> Self {
        Self::GenerationFailed(format!("{:?}", value))
    }
}

impl From<TextCompilerError> for BotError {
    fn from(value: TextCompilerError) -> Self {
        Self::GenerationFailed(format!("{:?}", value))
    }
}
