use mddd::macros::IConfig;

#[derive(IConfig)]
pub struct Config {
    #[description("tg bot token")]
    pub token: String,
    #[description("phrase trigger")]
    pub trigger: String,
    #[description("path to db with tokens")]
    pub content_db: String,
    #[description("path to db with settings")]
    pub settings_db: String,
    #[description("default command")]
    pub default_cmd: Option<String>,
    #[description("command for empty value")]
    pub empty_response_cmd: Option<String>,
    #[default(3000)]
    pub message_max_symbols: usize,
    #[default(10)]
    pub generate_phrase_max_tries: usize,
    #[default(3)]
    pub generation_depth: usize,
    #[default(1000)]
    pub text_compiler_chunk_size: usize,
    #[default(1.0)]
    pub sleep_on_errors: f64,
    #[prefix("MC_BOT_")]
    __meta__: (),
}
