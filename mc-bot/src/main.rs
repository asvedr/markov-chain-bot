mod app;
mod entities;
mod impls;
mod proto;
mod runner;
mod use_cases;

fn main() {
    use app::entrypoints::{cli, tg};
    use mddd::std::{LeafRunner, NodeRunner};

    NodeRunner::new("markov chain bot")
        .add("cli", LeafRunner::new(cli))
        .add("tg", LeafRunner::new(tg))
        .set_default_params(&["tg"])
        .run()
}
