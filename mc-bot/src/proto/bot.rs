use crate::entities::errors::BotError;
use easy_sqlite::errors::DbResult;

pub trait IBot {
    fn execute(&self, chat: &str, text: &str) -> Result<(&'static str, String), BotError>;
}

pub trait ICommand {
    fn name(&self) -> &'static str;
    fn check_match(&self, phrase: &str, tokens: &[&str]) -> bool;
    fn execute(&self, chat: &str, phrase: &str, tokens: &[&str]) -> Result<String, BotError>;
}

pub trait IChatSettingsRepo {
    fn get_depth(&self, chat: &str) -> DbResult<usize>;
    fn set_depth(&self, chat: &str, val: usize) -> DbResult<()>;
}

// -----------------------------
// --------- IMPLS -------------
// -----------------------------

impl<T: IBot> IBot for &T {
    fn execute(&self, chat: &str, text: &str) -> Result<(&'static str, String), BotError> {
        (**self).execute(chat, text)
    }
}

impl IBot for &dyn IBot {
    fn execute(&self, chat: &str, text: &str) -> Result<(&'static str, String), BotError> {
        (**self).execute(chat, text)
    }
}

impl<T: ICommand> ICommand for &T {
    fn name(&self) -> &'static str {
        (**self).name()
    }
    fn check_match(&self, phrase: &str, tokens: &[&str]) -> bool {
        (**self).check_match(phrase, tokens)
    }
    fn execute(&self, chat: &str, phrase: &str, tokens: &[&str]) -> Result<String, BotError> {
        (**self).execute(chat, phrase, tokens)
    }
}

impl ICommand for &dyn ICommand {
    fn name(&self) -> &'static str {
        (**self).name()
    }
    fn check_match(&self, phrase: &str, tokens: &[&str]) -> bool {
        (**self).check_match(phrase, tokens)
    }
    fn execute(&self, chat: &str, phrase: &str, tokens: &[&str]) -> Result<String, BotError> {
        (**self).execute(chat, phrase, tokens)
    }
}

impl ICommand for Box<dyn ICommand> {
    fn name(&self) -> &'static str {
        (**self).name()
    }
    fn check_match(&self, phrase: &str, tokens: &[&str]) -> bool {
        (**self).check_match(phrase, tokens)
    }
    fn execute(&self, chat: &str, phrase: &str, tokens: &[&str]) -> Result<String, BotError> {
        (**self).execute(chat, phrase, tokens)
    }
}

impl<T: IChatSettingsRepo> IChatSettingsRepo for &T {
    fn get_depth(&self, chat: &str) -> DbResult<usize> {
        (**self).get_depth(chat)
    }
    fn set_depth(&self, chat: &str, val: usize) -> DbResult<()> {
        (**self).set_depth(chat, val)
    }
}
