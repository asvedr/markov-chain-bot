use mc_base::proto::gen::GenSeqRequest;

use crate::entities::errors::BotError;

pub trait ITokenizer {
    fn tokenize(&self, text: &str) -> Vec<String>;
}

pub trait ITextGenerator {
    fn generate_text(
        &self,
        request: GenSeqRequest,
        check_valid: &impl Fn(&[&str]) -> bool,
    ) -> Result<String, BotError>;
}

// -----------------------------
// --------- IMPLS -------------
// -----------------------------

impl<T: ITextGenerator> ITextGenerator for &T {
    fn generate_text(
        &self,
        request: GenSeqRequest,
        check_valid: &impl Fn(&[&str]) -> bool,
    ) -> Result<String, BotError> {
        (**self).generate_text(request, check_valid)
    }
}
